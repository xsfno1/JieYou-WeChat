package com.xsf.jieyou.wechat.commons.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页模型
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 22:37
 * @see com.xsf.jieyou.wechat.commons.entity
 **/
@Data
public class PageResult<T> implements Serializable {
    private static final long serialVersionUID = 4946291486345975948L;

    /**
     * 当前页
     */
    private Integer pageIndex;
    /**
     * 总页数
     */
    private Integer totalPage;
    /**
     * 结果集
     */
    private List<T> result;
}
