package com.xsf.jieyou.wechat.commons.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DTO 数据传输对象。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/29 0029 21:04
 * @see com.xsf.jieyou.wechat.commons.dto
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseResult<T> implements Serializable {

    private static final long serialVersionUID = -1404691349493744873L;

    /**
     * 是否成功
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private boolean success;

    /**
     * 状态码
     */
    private Integer code;
    /**
     * 消息
     */
    private String message;
    /**
     * 返回数据
     */
    private T data;

    public ResponseResult(ResponseStatus responseStatus) {
        super();
        this.success = responseStatus.success();
        this.code = responseStatus.code();
        this.message = responseStatus.message();
    }

    public ResponseResult(ResponseStatus responseStatus, T data) {
        super();
        this.success = responseStatus.success();
        this.code = responseStatus.code();
        this.message = responseStatus.message();
        this.data = data;
    }

    public ResponseResult(Integer code) {
        super();
        this.code = code;
    }

    public ResponseResult(Integer code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public ResponseResult(Integer code, Throwable throwable) {
        super();
        this.code = code;
        this.message = throwable.getMessage();
    }

    public ResponseResult(Integer code, T data) {
        super();
        this.code = code;
        this.data = data;
    }

    public ResponseResult(Integer code, String message, T data) {
        super();
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static ResponseResult ok() {
        return ok(null);
    }

    public static ResponseResult ok(Object data) {
        return new ResponseResult<>(ResponseStatus.OK.code(), ResponseStatus.OK.message(), data);
    }

    public static ResponseResult fail() {
        return fail(null);
    }

    public static ResponseResult fail(Object data) {
        return new ResponseResult<>(ResponseStatus.FAIL.code(), ResponseStatus.FAIL.message(), data);
    }

    public static ResponseResult build(Integer code, String message) {
        return build(code, message, null);
    }

    public static ResponseResult build(Integer code, String message, Object data) {
        return new ResponseResult<>(code, message, data);
    }

    public String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", this.code);
        jsonObject.put("message", this.message);
        jsonObject.put("data", this.data);
        return JSON.toJSONString(jsonObject, SerializerFeature.DisableCircularReferenceDetect);
    }
}