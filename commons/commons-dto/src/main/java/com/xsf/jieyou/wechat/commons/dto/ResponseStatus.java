package com.xsf.jieyou.wechat.commons.dto;

/**
 * 自定义响应状态码
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/29 0029 21:04
 * @see com.xsf.jieyou.wechat.commons.dto
 **/
public enum ResponseStatus {
    /**
     * 账号密码错误
     */
    ADMIN_PASSWORD(100101, "账号密码错误"),
    /**
     * 用户 - 账号封停
     */
    ACCOUNT_SUSPENDED(100102, "账号封停"),
    /**
     * 用户 - 账号不存在
     */
    ACCOUNT_NOT_EXIST(100103, "账号不存在"),
    /**
     * 用户 - 用户名已存在
     */
    USERNAME_EXIST(100104, "用户名已存在"),
    /**
     * 用户 - 手机号已存在
     */
    PHONE_EXIST(100105, "手机号已存在"),
    /**
     * 管理员 - 不能操作自己 ：不能封停/解封自己帐号
     */
    CANNOT_HANDLE_YOUSELF(100200, "不允许对本身帐号进行业务操作"),
    /**
     * 用户 - 未登录
     */
    NOT_LOG_IN(100106, "未登录"),
    /**
     * 手机验证码错误
     */
    AUTH_CODE_ERROR(100107, "手机验证码错误"),
    /**
     * 短信发送异常
     */
    SEND_SMS_ERROR(100200, "短信发送异常"),
    /**
     * 未知错误
     */
    UNKNOWN(-1, "未知错误"),
    /**
     * 请求成功
     */
    OK(20000, "请求成功"),

    /**
     * 请求成功
     */
    SUCCESS(true, 20000, "请求成功"),
    /**
     * 请求失败
     */
    FAIL(20001, "请求失败"),
    /**
     * 请求失败
     */
    DEFEATED(false, 20001, "请求失败"),
    /**
     * 熔断请求
     */
    BREAKING(20002, "请求失败，请检查您的网络"),
    /**
     * 限流
     */
    CURRENT_LIMITING(20003, "当前访问人数过多，请稍后访问"),
    /**
     * 服务不存在
     */
    SERVICE_NOT_FOUND(40000, "服务不存在"),
    /**
     * 请求类型错误
     */
    METHOD_NOT_ALLOWED(40500, "请求类型错误！"),
    /**
     * 请求超时
     */
    REQUEST_TIMEOUT(40800, "请求超时！"),
    /**
     * 请求参数错误
     */
    REQUEST_PARAM_INVALID(41000, "请求参数错误！"),
    /**
     * 服务器内部错误
     */
    INTERNAL_SERVER_ERROR(50000, "服务器内部错误！服务暂时不可用！"),
    /**
     * 非法请求
     */
    ILLEGAL_REQUEST(50004, "非法请求"),
    /**
     * 获取令牌失败
     */
    GET_TOKEN_FAIL(50006, "令牌获取失败"),
    /**
     * 非法令牌
     */
    ILLEGAL_TOKEN(50008, "非法令牌"),
    /**
     * 无效令牌
     */
    INVALID_TOKEN(50010, "登录已过期，请重新登录！"),
    /**
     * 无令牌
     */
    NONE_TOKEN(50011, "尚未登录！"),
    /**
     * 其他客户登录
     */
    OTHER_CLIENTS_LOGGED_IN(50012, "其他客户登录"),
    /**
     * 令牌已过期
     */
    TOKEN_EXPIRED(50014, "登录已过期，请重新登录！"),
    /**
     * 权限不足
     */
    INSUFFICIENT_AUTHORITY(50016, "权限不足，无法访问！"),
    /**
     * 服务不可用
     */
    SERVICE_UNAVAILABLE(50300, "服务繁忙！");

    /**
     * 操作是否成功
     */
    boolean success;
    private Integer code;
    private String message;

    ResponseStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    ResponseStatus(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean success() {
        return success;
    }

    public Integer code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static String getMessage(int code) {
        for (ResponseStatus status : values()) {
            if (status.code().equals(code)) {
                return status.message();
            }
        }
        return null;
    }
}
