########################
# 分类数据
########################
import json
import random
from time import sleep

from pymysql import connect
import requests
from fake_useragent import UserAgent

url = "https://api.zbztb.cn/api/public/v1/categories"
headers = {
    "User-Agent": UserAgent().random
}

# 创建连接
client = connect(host='rm-uf611638579d3vi9kxo.mysql.rds.aliyuncs.com', port=3306, user='root', password='XSFandGEM548',
                 db='jieyou_wechat', charset='utf8')

# 获取游标
cursor = client.cursor()

resp = requests.get(url=url, headers=headers)
# resp.encoding = 'urf-8'  # 解决字符编码问题
cateData = json.loads(resp.text)["message"]


def cate_fun(data):
    for cate in data:

        try:
            # 如果没有叶子节点（为父节点），那么cate["children"]则不会异常，否则异常走catch方法
            # 目的：为了设置 is_parent 字段
            print(cate["cat_id"], cate["cat_name"], cate["cat_pid"], cate["cat_level"], cate["cat_deleted"],
                  cate["cat_icon"],cate["children"])
            sql = 'INSERT INTO tb_category(cat_id,cat_name,cat_pid,cat_level,cat_deleted,cat_icon,is_parent) VALUES ({},"{}",{},{},{},"{}",{})'.format(
                cate["cat_id"], cate["cat_name"], cate["cat_pid"], cate["cat_level"], cate["cat_deleted"],
                cate["cat_icon"],1)
        except:
            sql = 'INSERT INTO tb_category(cat_id,cat_name,cat_pid,cat_level,cat_deleted,cat_icon,is_parent) VALUES ({},"{}",{},{},{},"{}",{})'.format(
                cate["cat_id"], cate["cat_name"], cate["cat_pid"], cate["cat_level"], cate["cat_deleted"],
                cate["cat_icon"],0)

        cursor.execute(sql)

        try:
            if (len(cate["children"])):
                cate_fun(cate["children"])
        except:
            pass


cate_fun(cateData)

client.commit()
# 关闭数据库
cursor.close()
client.close()