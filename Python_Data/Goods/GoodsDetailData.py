########################
# 商品详情数据
########################
import json
from time import sleep

import math
import requests
from pymysql import connect
from fake_useragent import UserAgent

baseUrl = "https://api.zbztb.cn/api/public/v1/goods/detail"
headers = {
    "User-Agent": UserAgent().random
}
# 创建连接
client = connect(host='rm-uf611638579d3vi9kxo.mysql.rds.aliyuncs.com', port=3306, user='root', password='XSFandGEM548',
                 db='jieyou_wechat', charset='utf8')

# 获取游标
cursor = client.cursor()

# 获取所有商品ID
sql = 'SELECT id FROM tb_goods'
cursor.execute(sql)

ids = cursor.fetchall()

# 计步器
count = len(ids)

for index, gid in enumerate(ids):

    try:
        count -= 1

        # URL拼接
        url = "{}?goods_id={}".format(baseUrl, gid[0])

        resp = requests.get(url=url, headers=headers)
        content = json.loads(resp.text)["message"]

        # 插入商品详情字段
        sql = "INSERT INTO tb_goods_detail(goods_id,cat_id,goods_name,goods_price,goods_number,goods_weight,goods_big_logo,goods_small_logo,hot_mumber,cat_one_id,cat_two_id,cat_three_id,goods_introduce) VALUES ({},{},'{}',{},{},{},'{}','{}',{},{},{},{},'{}')".format(
            content["goods_id"],
            content["cat_id"],
            content["goods_name"],
            content["goods_price"],
            content["goods_number"],
            content["goods_weight"],
            content["goods_big_logo"],
            content["goods_small_logo"],
            content["hot_mumber"],
            content["cat_one_id"],
            content["cat_two_id"],
            content["cat_three_id"],
            content["goods_introduce"])

        cursor.execute(sql)

        for pic in content["pics"]:
            # 插入商品图片
            sql = 'INSERT INTO tb_goods_pic(pics_id,goods_id,pics_big,pics_mid,pics_sma,pics_big_url,pics_mid_url,pics_sma_url) VALUES ({},{},"{}","{}","{}","{}","{}","{}")'.format(
                pic["pics_id"],
                pic["goods_id"],
                pic["pics_big"],
                pic["pics_mid"],
                pic["pics_sma"],
                pic["pics_big_url"],
                pic["pics_mid_url"],
                pic["pics_sma_url"])
            cursor.execute(sql)

        print("{}% ：执行插入成功，剩余 {} 条商品记录。".format(math.ceil(index / count * 100), count))

        client.commit()
        sleep(0.2)
    except Exception as e:
        print(e)

print("全部执行完毕！！！")

# 关闭数据库
cursor.close()
client.close()
