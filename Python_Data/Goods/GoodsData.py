########################
# 商品数据
########################

import json
import random
from time import sleep

from pymysql import connect
import requests
from fake_useragent import UserAgent

baseUrl = "https://api.zbztb.cn/api/public/v1/goods/search"
headers = {
    "User-Agent": UserAgent().random
}
# 创建连接
client = connect(host='rm-uf611638579d3vi9kxo.mysql.rds.aliyuncs.com', port=3306, user='root', password='XSFandGEM548',
                 db='jieyou_wechat', charset='utf8')

total = 57445
pagenum = 1
pagesize = 20
totalPage = int(total / pagesize)

# 获取游标
cursor = client.cursor()
for pnum in range(totalPage):

    url = "{}?pagenum={}&pagesize={}".format(baseUrl, pnum + 1, pagesize)

    print("开始执行 {}页 的URL：{}".format(pnum + 1, url))

    resp = requests.get(url=url, headers=headers)
    # resp.encoding = 'urf-8'  # 解决字符编码问题
    goodsList = json.loads(resp.text)["message"]["goods"]

    # 提交事务
    for goods in goodsList:
        sql = 'INSERT INTO tb_goods(id,cat_id,goods_name,goods_price,goods_number,goods_big_logo,goods_small_logo) VALUES ({},{},"{}",{},{},"{}","{}")'.format(
            goods["goods_id"], goods["cat_id"], goods["goods_name"], goods["goods_price"], goods["goods_number"],
            goods["goods_big_logo"], goods["goods_small_logo"])
        cursor.execute(sql)

    print("{}% ：执行成功，剩余 {} 页记录。目前共插入了 {}条 商品记录。".format(int((pnum + 1) / totalPage * 100), totalPage - pnum + 1,
                                                        (pnum + 1) * pagesize))

    client.commit()
    sleep(0.2)

print("全部执行完毕！！！")

# 关闭数据库
cursor.close()
client.close()
