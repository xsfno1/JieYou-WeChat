// 封装的异步请求方法

/**
 * ajaxNums 存在原因：
 *    对于多个请求，如果第一个请求结束则会关闭所有加载中特效
 *    那么就需要判断是否全部请求结束才关闭加载中特效。
 */
let ajaxNums = 0;

export const request = (param) => {
  // 封装请求头
  let header = { ...param.header
  };
  // 判断 URL 中是否带有 /mine/ 需要权限验证的路径
  if (param.url.includes("/mine/")) {
    // 拼接 header 带上 token
    header["Authorization"] = wx.getStorageSync("token");
  }

  ajaxNums++;
  /**
   * 加载中ing 特效
   * 
   * 注意
   *  wx.showLoading 和 wx.showToast 同时只能显示一个
   *  wx.showLoading 应与 wx.hideLoading 配对使用
   */
  wx.showLoading({
    title: "加载中",
    mask: true
  })

  // 公共路径
  // const baseUrl = "http://xsfno1.top:8888/api/wechat";  // 我的阿里云
  const baseUrl = "https://xsfno1.top/api/wechat";  // 上线地址
  // const baseUrl = "http://127.0.0.1:8888/api/wechat";  // 本地配置
  // const baseUrl = "https://api.zbztb.cn/api/public/v1";   // 黑马易购
  return new Promise((resolve, reject) => {
    wx.request({
      ...param,
      url: param.url.startsWith("http") ? param.url:`${baseUrl}${param.url}`,
      success: (res) => {
        resolve(res.data);
      },
      fail: (error) => {
        reject(error);
      },
      complete: () => {
        ajaxNums--;
        if (ajaxNums === 0) {
          wx.hideLoading();
        }
      }
    })
  });
}