// components/Swiper/Swiper.js
Component({
  /**
   * 组件的属性列表，父传值
   */
  properties: {
    /* 轮播图 */
    swiper: {
      type: Object,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})