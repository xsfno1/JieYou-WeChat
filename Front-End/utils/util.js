const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/**
 * 设置TabBar购物车状态：更换TabBar图标
 */
const changeTabBarCartStatus = () => {
  // 1 查询缓存
  let cart = wx.getStorageSync("cart") || [];
  let iconPath = isCartFull() ? "/icon/cart2-man.png" : "/icon/cart2.png";
  let selectedIconPath = isCartFull() ? "/icon/cart2-man-ing.png" : "/icon/cart2-ing.png";
  wx.setTabBarItem({
    index: 2,
    iconPath,
    selectedIconPath
  })
}
/**
 * 判断购物车是否满了
 * @return true：满，false：空
 */
const isCartFull = () => {
  // 1 查询缓存
  let cart = wx.getStorageSync("cart") || [];
  return cart.length !== 0;
}

/**
 * JS实现sleep()方法,这种实现方式是利用一个伪死循环阻塞主线程。因为JS是单线程的。所以通过这种方式可以实现真正意义上的sleep()。
 */
const sleep = (delay) =>  {
  var start = (new Date()).getTime();
  while ((new Date()).getTime() - start < delay) {
    continue;
  }
}

module.exports = {
  formatTime,
  formatNumber,
  isCartFull,
  changeTabBarCartStatus,
  sleep
}