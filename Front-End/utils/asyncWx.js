/**
 * Promise 封装 WX 内置API - openSetting 打开设置
 */
export const openSetting = () => {
  return new Promise((resolve, reject) => {
    wx.openSetting({
      success: (res => {
        resolve(res);
      }),
      fail: (err => {
        reject(err);
      })
    })
  })
}
/**
 * Promise 封装 WX 内置API - getSetting 获取设置
 */
export const getSetting = () => {
  return new Promise((resolve, reject) => {
    wx.getSetting({
      success: (res => {
        resolve(res);
      }),
      fail: (err => {
        reject(err);
      })
    })
  })
}
/**
 * Promise 封装 WX 内置API - chooseAddress 获取用户地址
 */
export const chooseAddress = () => {
  return new Promise((resolve, reject) => {
    wx.chooseAddress({
      success: (res => {
        resolve(res);
      }),
      fail: (err => {
        reject(err);
      })
    })
  })
}
/**
 * Promise 封装 WX 内置API - showToast 显示消息提示框
 * 默认 mask:true
 */
export const showToast = (param) => {
  return new Promise((resolve, reject) => {
    wx.showToast({
      ...param,
      mask: true,
      success: (res => {
        resolve(res);
      }),
      fail: (err => {
        reject(err);
      })
    })
  })
}

/**
 * Promise 封装 WX 内置API - chooseImage 选择图片，成功会返回图片路径
 */
export const chooseImage = (param) => {
  return new Promise((resolve, reject) => {
    wx.chooseImage({
      ...param,
      success: (res => {
        resolve(res);
      }),
      fail: (err => {
        reject(err);
      })
    })
  })
}


/**
 * Promise 封装 WX 内置API - showModal 显示模态对话框
 */
export const showModal = (param) => {
  return new Promise((resolve, reject) => {
    wx.showModal({
      ...param,
      success: (res => {
        resolve(res);
      }),
      fail: (err => {
        reject(err);
      })
    })
  })
}

/**
 * Promise 封装 WX 内置API - scanCode 调起客户端扫码界面进行扫码
 */
export const scanCode = () => {
  return new Promise((resolve, reject) => {
    wx.scanCode({
      //是否只能从相机扫码，不允许从相册选择图片
      onlyFromCamera: true,
      //接口调用成功的回调函数
      success(res) {
        resolve(res);
      },
      fail: (err => {
        reject(err);
      })
    })
  })
}