import {
  changeTabBarCartStatus
} from "./utils/util.js";

App({

  globalData: {
    // 登录后跳转路径
    LoginRedirectTo: null,
    // 是否开启SMS
    IsOpenSMS: false,
  },

  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function() {
    // 购物车空满判断：更换TabBar图标
    changeTabBarCartStatus();
  },


  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function(options) {

  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function() {

  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function(msg) {

  }
})