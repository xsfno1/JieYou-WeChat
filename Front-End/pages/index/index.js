//获取应用实例
const app = getApp()

import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';
import {
  formatTime,
  formatNumber
} from '../../utils/util.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    /* 轮播图 */
    swiper: {
      list: [],
      autoplay: true,
      interval: 2000,
      duration: 500,
      circular: true
    },
    /* 分类导航 */
    cateNavList: [],
    /* 商品楼层 */
    floorList: [],
    /* 秒杀数据 */
    miaoshaData: {
      startTime: ['0', '8', '12', '16', '20'],
      timeIndex: 0,
      currentStartTime: '',
      countTime: {
        hour: '1',
        minute: '59',
        second: '59'
      }
    },
    // 秒杀商品列表
    seckillGoodsList: []
  },

  /* 定时器 */
  Timer: -1,
  MiaoshaStartTime: '',

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 获取轮播图数据
    this.getSwiperData();
    // 获取分类导航数据
    this.getCateNavData();
    // 获取楼层商品数据
    this.getFloorData();
    // 秒杀时间倒计时
    this.miaoshaTime();
    // 秒杀商品列表
    this.getSeckillGoodsList();
  },

  /**
   * 监听用户下拉刷新
   */
  onPullDownRefresh: function () {
    this.onLoad();
    wx.stopPullDownRefresh();
  },

  /**
   * 秒杀倒计时方法
   */
  miaoshaTime() {
    // 设置秒杀开始时间
    this.setMiaoshaStartTime();
    // 获取当前时间，同时得到活动结束时间数组
    const nowTime = new Date().getTime();
    // 对结束时间进行处理渲染到页面
    let endTime = new Date(this.MiaoshaStartTime).getTime();
    // 剩余时长
    let remainTime = (endTime - nowTime) / 1000
    this.timerFunc(remainTime);
    // 清除定时器
    clearInterval(this.Timer);
    // 开启定时器
    this.Timer = setInterval(() => {
      remainTime -= 1; // 时间减少1s
      if (remainTime > 0) {
        this.timerFunc(remainTime);
      } else {
        // 该秒杀场次已结束
        console.log("秒杀场次已结束...")
        // 清除定时器
        clearInterval(this.Timer);
        // 重新设置场次开始时间
        this.setMiaoshaStartTime();
        // 重新开启定时器
        this.miaoshaTime();
      }
    }, 1000);
  },
  /**
   * 定时器中执行的方法
   */
  timerFunc(remainTime) {
    // 获取天、时、分、秒
    let hour = parseInt(remainTime % (60 * 60 * 24) / 3600)
    let minute = parseInt(remainTime % (60 * 60 * 24) % 3600 / 60)
    let second = parseInt(remainTime % (60 * 60 * 24) % 3600 % 60)
    let miaoshaData = this.data.miaoshaData;
    miaoshaData["countTime"] = {
      hour: formatNumber(hour),
      minute: formatNumber(minute),
      second: formatNumber(second)
    };
    this.setData({
      miaoshaData
    })
  },

  /*设置每日秒杀场次开始时间  xx点场  每次相隔 4 小时*/
  setMiaoshaStartTime() {
    const nowDate = new Date();
    const year = nowDate.getFullYear();
    const month = nowDate.getMonth() + 1 < 10 ? "0" + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1;
    const hour = nowDate.getHours()
    // 场次判断
    let miaoshaData = this.data.miaoshaData;
    let miaoshaStartTime = miaoshaData.startTime;
    let index = miaoshaStartTime.findIndex(v => v > hour);
    let date = nowDate.getDate() < 10 ? "0" + nowDate.getDate() : nowDate.getDate();
    /**
     * 当index=-1时，代表着今日的秒杀场次全部结束
     */
    if (index === -1) {
      // 明日场次  date+1
      index = 0;
      date = nowDate.getDate() < 10 ? `0${nowDate.getDate() + 1}` : owDate.getDate() + 1;
    }
    this.MiaoshaStartTime = `${year}-${month}-${date} ${miaoshaStartTime[parseInt(index)]}:00:00`;
    // 设置场次
    miaoshaData["currentStartTime"] = miaoshaStartTime[index];
    miaoshaData["timeIndex"] = index;
    this.setData({
      miaoshaData
    })
  },


  // 获取秒杀商品数据
  async getSeckillGoodsList() {
    /**发送请求优化 Promise */
    let time = this.data.miaoshaData.startTime[this.data.miaoshaData.timeIndex]
    const res = await request({
      url: `/seckill/all/${time}`
    });
    this.setData({
      seckillGoodsList: res.data
    })
  },


  // 获取轮播图数据
  async getSwiperData() {
    /**发送请求优化 Promise */
    const res = await request({
      url: '/home/swiperdata'
    });
    let swiper = this.data.swiper;
    swiper["list"] = res.data
    this.setData({
      swiper
    })
  },


  // 获取分类导航数据
  async getCateNavData() {
    /**发送请求优化 Promise */
    const res = await request({
      url: '/home/catitems'
    });
    this.setData({
      cateNavList: res.data
    })
  },

  // 获取商品楼层数据
  async getFloorData() {
    /**发送请求优化 Promise */
    const res = await request({
      url: '/home/floordata'
    });
    this.setData({
      floorList: res.data
    })
  },



  /**
   * 用户点击右上角分享（index.js）
   */
  onShareAppMessage: function (ops) {
    if (ops.from === 'button') {
      // 来自页面内转发按钮
      console.log(ops.target)
    }
    return {
      title: '解忧杂货店小程序',
      path: 'pages/index/index', // 路径，传递参数到指定页面。
      imageUrl: 'https://gss2.bdstatic.com/9fo3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=8b194727aa44ad342ebf8081e8996bc9/4afbfbedab64034f62ca27cca4c379310b551df0.jpg', // 分享的封面图
      success: function (res) {
        // 转发成功
        console.log("转发成功:" + JSON.stringify(res));
      },
      fail: function (res) {
        // 转发失败
        console.log("转发失败:" + JSON.stringify(res));
      }
    }
  },
})