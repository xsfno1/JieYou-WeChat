// pages/order/order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // Tabs
    tabs: [{
        id: 0,
        value: "全部订单",
        isActive: true
      },
      {
        id: 1,
        value: "待付款",
        isActive: false
      },
      {
        id: 2,
        value: "待收货",
        isActive: false
      },
      {
        id: 3,
        value: "退款/退货",
        isActive: false
      },
    ],
    // 订单集合
    orderData: [],
    // 是否登录
    isLogined: false,
  },

  /**
   * Tabs 属性改变：子传父
   */
  handletabsItemChange(e) {
    const {
      index
    } = e.detail;
    // 改变Tabs下标
    this.changeTabsItem(index);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },


  /**
   * 生命周期函数--监听页面打开
   */
  onShow: function() {
    // ##########################  获取页面栈  ######################Start
    // onShow 方法中不存在 options 参数，那么我们可以通过页面栈来获取 options
    // 1 获取页面栈
    let pages = getCurrentPages();
    // 2 获取当前页面栈
    let currentPage = pages[pages.length - 1];
    // 3 获取 options
    let options = currentPage.options;
    // ##########################  获取页面栈  ######################  End


    // ##########################  Tabs下标值改变  ######################  Start
    // 1 获取传递过来的下标值参数，无则默认为0
    const type = options.type || 0;
    // 2 改变
    this.changeTabsItem(parseInt(type));
    // ##########################  Tabs下标值改变  ######################  End


    const userinfo = wx.getStorageSync("userinfo") || {};
    // ##########################  订单集合  ######################  Start
    // 是否登录
    if (Boolean(userinfo.nickName)) {
      let orderData = wx.getStorageSync("order");
      this.setData({
        orderData
      })
    }
    // ##########################  订单集合  ######################  End


    // ************************  是否登录  ************************ Start
    this.setData({
      isLogined: Boolean(userinfo.nickName)
    })
    // ************************  是否登录  ************************ End
  },

  /**
   * 封装一个改变Tabs下标的方法
   * @param 当前下标
   */
  changeTabsItem(index) {
    let tabs = this.data.tabs;
    tabs.forEach(v => v.isActive = v.id === index ? true : false);
    this.setData({
      tabs
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
})