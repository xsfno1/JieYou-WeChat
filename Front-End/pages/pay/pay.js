import {
  openSetting,
  getSetting,
  chooseAddress,
  showToast
} from "../../utils/asyncWx.js";
import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';
import {
  changeTabBarCartStatus
} from "../../utils/util.js";


Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 用户收货信息
    reviceInfo: {},
    // 支付商品集合
    payData: [],
    // 总价格
    totalPrice: 0,
    // 总数量
    totalNum: 0
  },

  /**
   * 监听获取收货地址按钮点击事件
   */
  async handleChooseAddress() {
    try {
      // 获取权限信息
      const res = await getSetting();
      // 用户曾经拒绝过授权(false)，那么就要引导用户到授权设置页面去赋予权限
      if (res.authSetting["scope.address"] === false) {
        await openSetting();
      }
      // 获取用户地址
      const res2 = await chooseAddress();
      // 存入 Data 中
      this.setData({
        reviceInfo: {
          userName: res2.userName,
          address: `${res2.provinceName}${res2.cityName}${res2.countyName}${res2.detailInfo}`,
          telNumber: res2.telNumber
        }
      });
      // 将用户收货地址存入缓存
      wx.setStorageSync("reviceInfo", this.data.reviceInfo);

    } catch (err) {
      console.error(err);
    }
  },

  /**
   * 在线支付
   */
  async handleToPay() {
    // *********** 判断 ***********
    // 1 是否有收货地址
    // 2 是否已登录
    const reviceInfo = wx.getStorageSync("reviceInfo");
    const userinfo = wx.getStorageSync("userinfo");
    if (!reviceInfo.userName) {
      await showToast({
        title: "收货信息不能为空",
        icon: 'none'
      });
      return;
    } else if (!userinfo.nickName) {
      wx.showModal({
        title: '未登录',
        content: '现在前往登录？',
        success(res) {
          if (res.confirm) {
            console.log("confirm");
            // 全局属性(登录后跳转问题)，因为switchTab路径不能传参
            getApp().globalData.LoginRedirectTo = "/pages/pay/pay";
            wx.switchTab({
              url: '/pages/mine/mine'
            })
          } else if (res.cancel) {
            console.log("cancel");
          }
        }
      })
      return;
    }
    // *********** 判断 ***********

    await showToast({
      title: "支付成功"
    })
    // 1 新增订单缓存
    let payData = this.data.payData || [];
    let orderData = wx.getStorageSync("order") || [];
    if (payData.length > 1) { // 表名订单有多个商品项
      let newPayData = [];
      payData.forEach(v => {
        let payItem = {};
        payItem["goodsId"] = v.goodsId;
        payItem["goodsSmallLogo"] = v.goodsSmallLogo;
        payItem["num"] = v.num;
        newPayData.push(payItem);
      })
      payData = newPayData;
    }
    orderData.unshift(payData);
    wx.setStorageSync("order", orderData);
    // 2 删除在线支付缓存
    wx.setStorageSync("pay", null);

    // 发送SMS短信通知(这里正常来说应该是后端的消息队列去完成)
    try{
      if (getApp().globalData.IsOpenSMS && reviceInfo.telNumber) {
        await request({
          url: `/sms/order/${reviceInfo.telNumber}`
        })
        console.log(`SMS:${reviceInfo.telNumber}`)
      }
    } catch(e){
      console.error(e);
    }

    // 返回我的订单页面
    wx.navigateTo({
      url: '/pages/order/order?type=0'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // **************   收货地址   ********************** Start
    const reviceInfo = wx.getStorageSync("reviceInfo") || {};
    this.setData({
      reviceInfo
    });
    // **************   收货地址   ********************** End


    // **************   支付订单商品集合   ********************** Start
    const payData = wx.getStorageSync("pay") || [];
    // 支付订单商品集合为空时会跳转到首页
    if (!payData.length) {
      wx.switchTab({
        url: '/pages/index/index',
      })
      return;
    }
    // 不为空
    this.setData({
      payData
    })
    // 设置总价格、数量
    this.setCartParam(payData);
    // **************   支付订单商品集合   ********************** Start
  },


  /**
   * 封装的一个计算购物车商品总价和数量
   * @param payData 购物车数据
   */
  setCartParam(payData) {
    //计算总价、总数量
    let totalPrice = 0;
    let totalNum = 0;
    payData.forEach(v => {
      totalPrice += v.goodsPrice * v.num;
      totalNum += v.num;
    })

    this.setData({
      totalPrice,
      totalNum
    });
  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },


})