// pages/goods_list/goods_list.js
import {
  request
} from "../../request/index.js";


Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 返回顶部按钮是否隐藏
    isBack2TopHidden: true,
    // 选项卡
    tabs: [{
        id: 0,
        value: "综合",
        filed: "",
        isActive: true
      },
      {
        id: 1,
        value: "销量",
        filed: "goodsName",  // 这里因为缺失了销量属性，所以通过商品名称来强凑
        isActive: false
      },
      {
        id: 2,
        value: "价格",
        filed: 'goodsPrice',
        isActive: false
      }
    ],
    // 商品列表数据
    goodsList: [],
  },

  // 查询商品列表接口所需要的参数
  QueryParam: {
    query: "",
    cid: "",
    orderby: "",
    pagenum: 1,
    pagesize: 20
  },
  // 总页数
  TotalPage: 0,

  /**
   * 子传父
   */
  handleTabsChange(event) {
    // 0 重置数据
    this.resetPagingData();

    // 1 下标处理
    const {
      index
    } = event.detail;
    let {
      tabs
    } = this.data;
    // 选中
    tabs.forEach((v, i) =>
      v.isActive = v.id === index ? true : false
    );
    this.setData({
      tabs
    })

    // 2 获取对应商品列表
    this.QueryParam.orderby = tabs[index].filed
    this.getGoodsListByCid(0);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 设置参数
    this.QueryParam.cid = options.cid || "";
    this.QueryParam.query = options.query || "";
    this.QueryParam.pagenum = options.pagenum || this.QueryParam.pagenum;
    this.QueryParam.pagesize = options.pagesize || this.QueryParam.pagesize;

    // 获取商品列表数据
    this.getGoodsListByCid();
  },

  /**
   * 返回顶部点击事件
   */
  handleBack2Top() {
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 300
    })
  },

  /**
   * 监听窗口滑动事件
   */
  onPageScroll(e) {
    let isBack2TopHidden = e.scrollTop > 300 ? false : true;
    // 性能优化，减少更新频率
    if (this.data.isBack2TopHidden !== isBack2TopHidden) {
      this.setData({
        isBack2TopHidden
      })
      console.log(this.data.isBack2TopHidden)
    }
  },

  /**
   * 监听用户下拉刷新
   */
  onPullDownRefresh: function() {
    // 重置数据
    this.resetPagingData();


    // 显示顶部刷新图标
    wx.showNavigationBarLoading();

    // 重新获取商品列表数据
    // this.QueryParam.pagenum = 2;  // 模拟刷新效果
    this.getGoodsListByCid(null,function() {
      // 隐藏顶部刷新图标
      wx.hideNavigationBarLoading();
      // 停止下拉事件
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 上拉触底事件，查询下一页数据
   *  1 获取总页数
   *  2 判断是否存在下一页
   */
  onReachBottom: function() {
    console.log("上拉触底事件，查询下一页数据");
    // 判断是否存在下一页
    if (this.QueryParam.pagenum >= this.TotalPage) {
      // 否，最后一页
      wx.showToast({
        title: '已经是最后一页了',
      })
    } else {
      // 是，获取下一页数据
      this.QueryParam.pagenum++;
      this.getGoodsListByCid();
    }
  },


  /**
   * 获取商品列表数据
   * @param flag 0:列表不叠加，1:列表叠加
   * @param callback 回调函数
   */
  async getGoodsListByCid(flag,callback) {
    try {
      const res = await request({
        url: '/goods/search',
        data: this.QueryParam
      });
      let data = res.data;
      let goodsList = [];
      // 列表不叠加
      if(flag === 0){
        goodsList = data.result
      }
      // 列表叠加
      else{
        goodsList = [...this.data.goodsList, ...data.result]
      }
      this.setData({
        goodsList
      })
      // 设置总页数 
      this.TotalPage = data.totalPage;
    } catch (e) {
      console.error(e);
    }

    if (callback !== null && typeof callback === "function") {
      // 回调函数
      callback();
    }

  },

  /**
   * 重置分页数据
   */
  resetPagingData(){
    // 重置数据
    this.setData({
      goodsList: []
    })
    this.QueryParam.pagenum = 1;
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
})