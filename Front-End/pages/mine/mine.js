import {
  showToast,
  scanCode
} from "../../utils/asyncWx.js";
import {
  request
} from "../../request/index.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 用户信息
    userInfo: {},
    // 收藏菜单信息
    collectInfo: [{
        id: 0,
        url: "/pages/collect/collect?type=0",
        num: 0,
        name: "收藏的店铺"
      },
      {
        id: 1,
        url: "/pages/collect/collect?type=1",
        num: 0,
        name: "收藏的商品"
      },
      {
        id: 2,
        url: "/pages/collect/collect?type=2",
        num: 0,
        name: "关注的商品"
      },
      {
        id: 3,
        url: "/pages/collect/collect?type=3",
        num: 0,
        name: "我的足迹"
      },
    ]
  },


  /**
   * 登录
   */
  handleLogin(e) {
    console.log(e)
    // 获取用户信息
    let {
      userInfo
    } = e.detail
    this.setData({
      userInfo
    })
    // 存入缓存中
    wx.setStorageSync("userinfo", userInfo);
    // 登录后重定向位置
    var loginRedirectTo = getApp().globalData.LoginRedirectTo;
    getApp().globalData.LoginRedirectTo = null; // 重置
    if (loginRedirectTo != null && loginRedirectTo.length) {
      wx.redirectTo({
        url: loginRedirectTo,
      })
    }
  },

  /**
   * 扫一扫功能
   */
  async handleScanQRCode(e) {
    let {
      errMsg,
      result
    } = await scanCode()
    //二维码唯一标识
    const code = result.split("?code=")[1]
    // 扫码成功
    console.log("errMsg:",errMsg)
    console.log("result:", result)
    if (errMsg === "scanCode:ok") {
      // 跳转到后端服务器,表示已经扫码成功
      const {
        data
      } = await request({
        url: `/qrcode/scan?code=${code}`
      });
      console.log("scan response:", data)
      // 跳转授权登录界面,授权登录  code:后端传递的唯一码
      wx.navigateTo({
        url: `/pages/authorization/authorization?code=${code}`
      })
    }
  },

  /**
   * 监听清除缓存按钮点击
   */
  async handleClearStorageSync(e) {
    try {
      wx.clearStorageSync();
      await showToast({
        title: "退出登录成功"
      })
      // 刷新
      this.onShow();
    } catch (e) {
      await showToast({
        title: "退出登录失败",
        icon: "none"
      })
      console.error(e);
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // ************************* 用户信息 ************************* Start
    // 获取缓存中的用户信息
    const userInfo = wx.getStorageSync("userinfo") || {};
    this.setData({
      userInfo
    })
    // ************************* 用户信息 ************************* End


    // **************  获取缓存中的收藏信息  Start
    const collect = wx.getStorageSync("collect") || {};
    if (collect.length !== 0) {
      let collectInfo = this.data.collectInfo;
      collectInfo[0]["num"] = collect.shops ? collect.shops.length : 0;
      collectInfo[1]["num"] = collect.goods ? collect.goods.length : 0;
      collectInfo[2]["num"] = collect.concerns ? collect.concerns.length : 0;
      collectInfo[3]["num"] = collect.history ? collect.history.length : 0;
      this.setData({
        collectInfo
      })
    }
    // **************  获取缓存中的收藏信息  End
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
})