import {
  request
} from "../../request/index.js";
import {
  showToast,
  showModal
} from "../../utils/asyncWx.js";
import {
  isCartFull,
  changeTabBarCartStatus,
  sleep
} from "../../utils/util.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';


Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 购物车空满
    isCartFullFlag: false,
    // 商品对象
    goodsObj: {},
    // 是否已收藏
    isCollect: false,
    // 参与秒杀
    seckillInfo: {},
  },

  // 商品信息
  GoodsInfo: {},

  // 是否要测试限流功能
  IsTestLimit: true,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let {
      goods_id,
      new_price,
      old_price,
      is_seckilling
    } = options

    // 获取商品详情
    this.getgoodsObj(goods_id, function() {
      // ############# 浏览足迹 ############### Start
      const userinfo = wx.getStorageSync("userinfo")||{};
      if (userinfo.nickName){
        // 1 获取缓存
        let collect = wx.getStorageSync("collect") || {};
        let history = collect["history"] || [];
        // 2 添加，判断，最大长度为10条
        if (history.length >= 10) {
          // 删除最后一条
          history.pop();
        }
        history.unshift(this.GoodsInfo);
        // 3 存储
        collect["history"] = history;
        wx.setStorageSync("collect", collect);
      }
      // ############# 浏览足迹 ############### End

      // **************************** 参与秒杀 Start
      if (is_seckilling != null && new_price != null) {
        let seckillInfo = {};
        let goodsObj = this.data.goodsObj || {};
        seckillInfo["oldPrice"] = old_price || goodsObj["goodsPrice"] || 999;
        seckillInfo["isSeckilling"] = is_seckilling;
        goodsObj["goodsPrice"] = new_price;
        this.GoodsInfo["goodsPrice"] = new_price;
        this.setData({
          seckillInfo,
          goodsObj
        })
      }
      // **************************** 参与秒杀 End
    }.bind(this));

  },
  /**
   * 生命周期函数--监听页面打开
   */
  onShow: function() {
    // ************************  获取页面栈  ************************ Start
    // 1 获取页面栈
    let pages = getCurrentPages();
    // 2 获取当前页面栈
    let currentPage = pages[pages.length - 1];
    // 3 获取 options
    let options = currentPage.options;
    // ************************  获取页面栈  ************************ End


    // ************************  商品收藏 and 购物车图标  ************************ Start
    // 获取ID
    const id = parseInt(options.goods_id);
    // 收藏操作========================================================
    // 1 获取收藏列表对象 -- 商品收藏
    let collect = wx.getStorageSync("collect").goods || [];
    // 2 判断该商品是否存在于收藏队列
    let isCollect = collect.length && collect.findIndex(v => v.goodsId === id) !== -1;
    // 3 改变Data
    this.setData({
      isCollect,
      // 购物车图标(空满)===============================================
      isCartFullFlag: isCartFull()
    })
    // ************************  商品收藏 and 购物车图标  ************************ Start
  },

  /**
   * 获取商品详情
   * @param goods_id 商品ID
   * @callback 回调函数
   */
  async getgoodsObj(goods_id, callback) {
    const res = await request({
      url: `/goods/detail/${goods_id}`
    });
    const {
      goodsId,
      goodsName,
      goodsPrice,
      goodsSmallLogo,
      pics,
      goodsIntroduce
    } = res.data;
    this.GoodsInfo = {
      goodsId,
      goodsName,
      goodsPrice,
      goodsSmallLogo
    }
    this.setData({
      /**
       * Data中存放的数据是前台页面所用得到的，而API接口传的数据大部分用不到，所以就需要封装下数据。
       */
      goodsObj: {
        goodsPrice,
        pics,
        goodsName,
        goodsIntroduce
      }
    })

    if (callback !== null && typeof callback === "function") {
      // 回调函数
      callback();
    }

  },

  /**
   * 鼠标点击方法图片预览
   */
  handlePrevewImage(event) {
    const urls = this.data.goodsObj.pics.map(v => v.picsMidUrl);
    const current = event.currentTarget.dataset.current;
    wx.previewImage({
      current,
      urls
    })
  },

  /**
   * 加入购物车
   */
  handleAddCart() {
    // 1 查询缓存
    let cart = wx.getStorageSync("cart") || [];
    // 2 判断商品是否存在于购物车中
    let index = cart.findIndex(v => v.goodsId === this.GoodsInfo.goodsId);
    if (index === -1) {
      // 2.1 不存在 第一次添加
      this.GoodsInfo.num = 1; //商品数量
      this.GoodsInfo.checked = true; //商品选中，购物车中需要用到
      cart.push(this.GoodsInfo)
    } else {
      // 2.2 存在
      cart[index].num++;
    }

    wx.setStorageSync("cart", cart);

    // 添加成功提示
    wx.showToast({
      title: '加入购物车成功',
      icon: "success",
      mask: true
    })

    // 购物车空满判断：更换TabBar图标
    changeTabBarCartStatus();
    // 商品详情页工具栏购物车图标(空满)============================
    this.setData({
      isCartFullFlag: true
    })
  },

  /**
   * 立即购买
   */
  async handleToBuy(e) {
    let payData = [];
    let goodsItem = this.GoodsInfo;
    goodsItem["num"] = 1;
    payData.unshift(goodsItem);
    // 选中的商品集合（支付商品项）
    wx.setStorageSync("pay", payData);


    // ******************    模拟限流 Start    *************************
    if (this.data.seckillInfo.isSeckilling && this.data.seckillInfo.isSeckilling === 'true' && this.IsTestLimit) {
      const that = this;
      wx.showModal({
        title: '限流功能测试中',
        content: '是否要终止测试？',
        success(res) {
          if (res.confirm) {
            console.log("跳出测试")
            that.IsTestLimit = false;
          } else if (res.cancel) {
            console.log("继续测试")
            that.IsTestLimit = true;
          }
        }
      })

      while (this.IsTestLimit) {
        const res = await request({
          url: "/seckill/limit"
        })
        // 响应码为 20003 时：触发限流，模拟限流成功
        console.log(res.code, res.message);
        await showToast({
          title: `${res.code}:${res.message}`,
          icon: "none"
        })
        if (res.code === 20003) {
          const res2 = await showModal({
            title: '限流功能测试成功',
            content: '是否要终止测试？'
          })
          if (res2.confirm) {
            this.IsTestLimit = false;
            console.log("终止测试")
          } else if (res2.cancel) {
            this.IsTestLimit = true;
            console.log("继续测试")
            
            wx.showModal({
              title: '限流功能测试中',
              content: '是否要终止测试？',
              success(res) {
                if (res.confirm) {
                  console.log("跳出测试")
                  that.IsTestLimit = false;
                } else if (res.cancel) {
                  console.log("继续测试")
                  that.IsTestLimit = true;
                }
              }
            })
          }
        }
        sleep(1000);
      }
    }
    // ******************    模拟限流 End    *************************

    // 3 跳转到字符页面
    wx.navigateTo({
      url: '/pages/pay/pay',
    });
  },


  /**
   * 收藏按钮监听
   */
  async handleItemCollect(e) {
    // 是否已收藏
    let isCollect = !this.data.isCollect;
    // ID
    const id = this.GoodsInfo.goodsId;

    // 1 获取缓存中的收藏列表对象
    let collect = wx.getStorageSync("collect") || {};
    // 商品收藏对象
    let goodsCollect = collect.goods || [];
    // 商品下标
    let index = goodsCollect.findIndex(v => v.goodsId === id);
    // 2 判断是否存在该收藏对象
    if (index !== -1) {
      // 存在，从收藏对象中删除
      goodsCollect.splice(index, 1);
      await showToast({
        title: "取消收藏成功",
        icon: "success"
      });
    } else {
      // 不存在，添加进收藏
      goodsCollect.push(this.GoodsInfo);
      await showToast({
        title: "收藏成功",
        icon: "success"
      });
    }
    // 3 改变Data 并 将收藏对象存入缓存中
    collect["goods"] = goodsCollect;
    wx.setStorageSync("collect", collect);
    this.setData({
      isCollect
    })
  },

  /**
   * 生命周期回调—监听页面卸载
   */
  onUnload: function () {
    this.IsTestLimit = false;
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
})