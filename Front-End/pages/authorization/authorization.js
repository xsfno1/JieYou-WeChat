import {
  showToast
} from "../../utils/asyncWx.js";
import {
  request
} from "../../request/index.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //二维码内容的地址
    code: ""
  },

  /**
   * 授权登录
   */
  async handleConfirmAuthor(e) {
    // 获取缓存中的用户对象
    const userinfo = wx.getStorageSync("userinfo") || {};
    const {
      code
    } = this.data
    const isAuthor = e.currentTarget.dataset['isauthor'];
    const res = await request({
      method: "POST",
      url: `/qrcode/author/${code}/${isAuthor}`,
      data: JSON.stringify(userinfo)
    });
    console.log(res)
    // 跳转到我的中心
    wx.switchTab({
      url: '/pages/mine/mine',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      code: options.code
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

})