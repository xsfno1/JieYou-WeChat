import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';


Page({

  /**
   * 页面的初始数据
   */
  data: {
    /* 秒杀数据 */
    miaoshaData: {
      startTime: ['0', '8', '12', '16', '20'],
      timeIndex: 0,
    },
    // 是不是当前在开抢中
    isSecKilling: true,
    // 秒杀商品列表数据
    goodsList: [],
    // 返回顶部按钮是否隐藏
    isBack2TopHidden: true,
    // 头部标题栏
    title: [{
        time: "最后疯抢",
        sub_title: "已开抢",
        isActive: false
      },
      {
        time: "00:00",
        sub_title: "已开抢",
        isActive: false
      },
      {
        time: "08:00",
        sub_title: "已开抢",
        isActive: false
      },
      {
        time: "12:00",
        sub_title: "即将开抢",
        isActive: false
      },
      {
        time: "16:00",
        sub_title: "即将开抢",
        isActive: false
      },
      {
        time: "20:00",
        sub_title: "即将开抢",
        isActive: false
      }
    ],
  },
  // 场次下标
  TimeIndex: 0,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    let tindex = options.tindex;
    if (!tindex) {
      this.setMiaoshaStartTime();
      tindex = this.TimeIndex;
    }
    this.TimeIndex = tindex || this.TimeIndex;

    tindex--;
    let {
      title
    } = this.data;
    if (tindex < 0) {
      tindex = title.length - 1 - 1
    }
    let afterArray = title.splice(1, tindex);
    afterArray.forEach(v => v.sub_title = "明日疯抢");
    title.forEach((v, i) => v.sub_title = i === 0 || i === 1 ? "已开抢" : "即将开抢");

    title = [...title, ...afterArray];
    this.setData({
      title
    })

    // 获取秒杀商品列表数据
    this.getGoodsListByData(parseInt(this.data.title[tindex]["time"].split(":")[0]));

    // 修改选择下标，第1个，因为第0个是 最后的疯抢
    this.changeSeckillTitleIndex(1);
  },


  /*设置每日秒杀场次开始时间]*/
  setMiaoshaStartTime() {
    const nowDate = new Date();
    const year = nowDate.getFullYear();
    const month = nowDate.getMonth() + 1 < 10 ? "0" + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1;
    const hour = nowDate.getHours()
    // 场次判断
    let miaoshaData = this.data.miaoshaData;
    let miaoshaStartTime = miaoshaData.startTime;
    let index = miaoshaStartTime.findIndex(v => v > hour);
    let date = nowDate.getDate() < 10 ? "0" + nowDate.getDate() : nowDate.getDate();
    // 设置场次
    this.TimeIndex = index;
  },


  /**
   * 返回顶部点击事件
   */
  handleBack2Top() {
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 300
    })
  },

  /**
   * 监听窗口滑动事件
   */
  onPageScroll(e) {
    let isBack2TopHidden = e.scrollTop > 300 ? false : true;
    // 性能优化，减少更新频率
    if (this.data.isBack2TopHidden !== isBack2TopHidden) {
      this.setData({
        isBack2TopHidden
      })
      console.log(this.data.isBack2TopHidden)
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    
  },

  /**
   * 获取商品列表数据
   * @param time 场次
   */
  async getGoodsListByData(time) {
    const res = await request({
      url: `/seckill/all/${time}`
    });
    this.setData({
      goodsList: res.data
    })
  },

  /**
   * 监听标题下标值改变事件
   */
  handleTitleIndexChange(event) {
    const {
      index
    } = event.currentTarget.dataset;
    this.changeSeckillTitleIndex(index);
  },

  /**
   * 封装的一个修改秒杀下标的方法，下标值改变，商品列表数据也会改变
   * @param index 当前选择下标
   */
  changeSeckillTitleIndex(index) {
    let title = this.data.title;
    title.forEach((v, i) => v.isActive = i === index ? true : false);

    // 判断该标题栏商品是否开抢中
    let isSecKilling = false;
    if (index === 1 || index === 0) {
      isSecKilling = true;
    }

    // 秒杀商品值改变
    let tindex = index;
    if (tindex === 0) {
      tindex = this.data.title.length - 1
    }
    this.getGoodsListByData(parseInt(this.data.title[tindex]["time"].split(":")[0]));

    this.setData({
      title,
      isSecKilling
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
})