// pages/cart/cart.js
import {
  openSetting,
  getSetting,
  chooseAddress,
  showToast
} from "../../utils/asyncWx.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';
import {
  changeTabBarCartStatus
} from "../../utils/util.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 用户收货信息
    reviceInfo: {},
    // 购物车商品项
    cartData: [],
    // 所有商品项都被选中
    allChecked: false,
    // 总价
    totalPrice: 0,
    // 总数量
    totalNum: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面打开
   */
  onShow: function(options) {
    // 缓存中的地址信息
    const reviceInfo = wx.getStorageSync("reviceInfo") || {};
    // 购物车商品项
    const cartData = wx.getStorageSync("cart") || [];
    // 计算全选
    // every：数组方法，会遍历，会接受一个回调函数，那么，每一个回调函数都会返回true，那么every方法的返回值为true
    // 只要有一个回调函数返回false，那么不再循环执行，直接返回false
    // 空数组调用every，直接返回true
    // let allChecked = cartData.length ? cartData.every(v => v.checked) : false;

    //5 重新计算价格数量属性
    this.setCartParam(cartData);

    this.setData({
      reviceInfo
    });

    // 购物车空满判断：更换TabBar图标
    changeTabBarCartStatus();
  },


  /**
   * 监听获取收货地址按钮点击事件
   */
  async handleChooseAddress() {
    try {
      // 获取权限信息
      const res = await getSetting();
      // 用户曾经拒绝过授权(false)，那么就要引导用户到授权设置页面去赋予权限
      if (res.authSetting["scope.address"] === false) {
        await openSetting();
      }
      // 获取用户地址
      const res2 = await chooseAddress();
      // 存入 Data 中
      this.setData({
        reviceInfo: {
          userName: res2.userName,
          address: `${res2.provinceName}${res2.cityName}${res2.countyName}${res2.detailInfo}`,
          telNumber: res2.telNumber
        }
      });
      // 将用户收货地址存入缓存
      wx.setStorageSync("reviceInfo", this.data.reviceInfo);

    } catch (err) {
      console.error(err);
    }
  },

  /**
   * 商品选中框操作
   */
  handleItemChange(event) {
    // 1 获取操作的商品ID
    const {
      id
    } = event.currentTarget.dataset
    // 2 获取购物车数据
    let cartData = this.data.cartData || [];
    // 3 获取该商品在购物车对象中的下标
    let index = cartData.findIndex(v => v.goodsId === id);
    if (index !== -1) {
      cartData[index].checked = !cartData[index].checked
    }
    //4 重新计算价格数量属性并购物车存入缓存
    this.setCartParam(cartData);
  },
  /**
   * 全选框，所有购物车商品选中
   */
  handleAllItemChoose(event) {
    // 1 获取购物车数据
    let cartData = this.data.cartData || [];
    if (!cartData.length) {
      // 购物车为空，全选框操作无效
      this.setData({
        allChecked: false
      })
      return;
    }
    // 2 获取全选Data
    let allChecked = this.data.allChecked;
    // 3 遍历，修改选中字段
    cartData.forEach(v => v.checked = !allChecked);
    // 4 计算购物车商品总价和数量的方法
    this.setCartParam(cartData);
  },
  /**
   * 商品数量修改
   */
  handleGoodsNumEdit(event) {
    // 1 获取操作的商品ID，和操作符(1,-1)
    const {
      id,
      operation
    } = event.currentTarget.dataset;
    // 2 获取购物车数据
    let cartData = this.data.cartData || [];
    // 3 获取该商品在购物车对象中的下标
    let index = cartData.findIndex(v => v.goodsId === id);
    if (index !== -1) {
      if (cartData[index].num <= 1 && operation === -1) {
        // 界限判定
        wx.showModal({
          title: '删除商品',
          content: '是否需要从购物车中移除该商品？',
          success: (res) => {
            if (res.confirm) {
              // 确认移除
              cartData.splice(index, 1);
              //4 重新计算价格数量属性并购物车存入缓存
              this.setCartParam(cartData);
              // 购物车空满判断：更换TabBar图标
              changeTabBarCartStatus();
            } else if (res.cancel) {

            }
          }
        })
      } else {
        cartData[index].num += operation
        //4 重新计算价格数量属性并购物车存入缓存
        this.setCartParam(cartData);
      }
    }
  },

  /**
   * 结算
   */
  async handleToBuy(event) {
    // 1 判断地址和商品数量
    const {
      reviceInfo,
      totalNum,
      cartData
    } = this.data
    if (!reviceInfo.userName) {
      await showToast({
        title: "收货信息不能为空",
        icon: 'none'
      });
      return;
    } else if (!totalNum) {
      await showToast({
        title: "商品不能为空",
        icon: 'none'
      });
      return;
    }
    // 2 缓存
    // 选中的商品集合（支付商品项）
    let payData = cartData.filter(v => v.checked);
    wx.setStorageSync("pay", payData);
    // 未选中的商品结合，重新赋值
    let newCartData = cartData.filter(v => !v.checked);
    this.setData({
      cartData: newCartData
    })
    wx.setStorageSync("cart", newCartData);
    // 3 跳转到字符页面
    wx.navigateTo({
      url: '/pages/pay/pay',
    });
  },


  /**
   * 封装的一个计算购物车商品总价和数量,还有购物车回缓存的方法
   * @param cartData 购物车数据
   */
  setCartParam(cartData) {
    let allChecked = true;
    //计算总价、总数量
    let totalPrice = 0;
    let totalNum = 0;
    cartData.forEach(v => {
      if (v.checked) {
        totalPrice += v.goodsPrice * v.num;
        totalNum += v.num;
      } else {
        allChecked = false;
      }
    })
    // 如果数组为空的情况下，对全选再做一次判断
    allChecked = cartData.length ? allChecked : false;

    // 重新存入缓存
    wx.setStorageSync("cart", cartData);

    this.setData({
      cartData,
      allChecked,
      totalPrice,
      totalNum
    });
  },

  /**
     * 用户点击右上角分享
     */
  onShareAppMessage: function () {

  },
})