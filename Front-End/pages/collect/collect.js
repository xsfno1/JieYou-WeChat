// pages/collect/collect.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // Tabs
    tabs: [{
        id: 0,
        value: "店铺收藏",
        isActive: true
      },
      {
        id: 1,
        value: "商品收藏",
        isActive: false
      },
      {
        id: 2,
        value: "商品关注",
        isActive: false
      },
      {
        id: 3,
        value: "浏览足迹",
        isActive: false
      },
    ],
    // 收藏列表
    collectList: [],
    // 是否登录
    isLogined : false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // ************************  获取页面栈  ************************ Start
    // onShow 方法中不存在 options 参数，那么我们可以通过页面栈来获取 options
    // 1 获取页面栈
    let pages = getCurrentPages();
    // 2 获取当前页面栈
    let currentPage = pages[pages.length - 1];
    // 3 获取 options
    let options = currentPage.options;
    // ************************  获取页面栈  ************************ End


    // ************************  Tabs  ************************ Start
    const type = parseInt(options.type) || 0;
    // 改变Tabs下标
    this.changeTabsItem(type);
    // ************************  Tabs  ************************ End

    const userinfo = wx.getStorageSync("userinfo") || {};
    // ************************  收藏列表获取  ************************ Start
    // 是否登录
    if (Boolean(userinfo.nickName)){
      this.getCollectList(type);
    }
    // ************************  收藏列表获取  ************************ End


    // ************************  是否登录  ************************ Start
    this.setData({
      isLogined: Boolean(userinfo.nickName)
    })
    // ************************  是否登录  ************************ End
  },


  /**
   * Tabs 属性改变：子传父
   */
  handletabsItemChange(e){
    const {
      index
    } = e.detail;
    // 改变Tabs下标
    this.changeTabsItem(index);
  },

  /**
   * 封装一个改变Tabs下标的方法
   * @param 当前下标
   */
  changeTabsItem(index) {
    let tabs = this.data.tabs;
    tabs.forEach(v => v.isActive = v.id === index ? true : false);
    this.setData({
      tabs
    });

    // ************************  收藏列表获取  ************************ Start
    this.getCollectList(index);
    // ************************  收藏列表获取  ************************ End
  },

  /**
   * 获取收藏列表
   * @param index(number类型) Tabs下标
   */
  getCollectList(index){
    // 1 获取缓存
    let collectList = [];
    // 商品收藏
    if (index === 1) {
      collectList = wx.getStorageSync("collect")["goods"] || [];
    } 
    // 浏览足迹
    else if (index === 3) {
      collectList = wx.getStorageSync("collect")["history"];
    }
    // 2 添加Data
    this.setData({
      collectList
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
})