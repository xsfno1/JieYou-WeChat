// pages/aboutme/aboutme.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 作者信息
    authorInfo : [
      { key: "昵称",val : "小帅发"},
      { key: "手机号", val: "13763055759" },
    ],
    // 商业化组件
    busiListData: [{
        "code": "Aliyun SMS",
        "text": "阿里云短信服务",
      },
      {
        "code": "Aliyun AHAS",
        "text": "阿里云应用高可用服务",
      },
    ],
    // 后端技术
    backListData: [{
        "code": "Spring Boot",
        "text": "新一代 JavaEE 开发标准",
      },
      {
        "code": "Spring Cloud Alibaba",
        "text": "阿里巴巴基于 Spring Cloud 编程模型的微服务生态",
      },
      {
        "code": "Spring Cloud Alibaba Dubbo",
        "text": "与 Spring Cloud Alibaba 生态相结合的高性能 Java RPC 框架",
      },
      {
        "code": "Spring Cloud Gateway",
        "text": "服务网关路由代理",
      },
      {
        "code": "SSM",
        "text": "目前流行的web项目框架",
      },
      {
        "code": "TkMyBatis",
        "text": "基于 MyBatis 二次开发的轻量级框架，用于简化 MyBatis 操作",
      },
      {
        "code": "HikariCP",
        "text": "号称史上最快的数据库连接池",
      },
      {
        "code": "MybatisCodeHelper",
        "text": "Intellij IDEA 插件，用于 MyBatis 相关代码生成",
      },
      {
        "code": "PageHelper",
        "text": "号称史上最快的数据库连接池",
      }, {
        "code": "HikariCP",
        "text": "MyBatis 分页插件",
      }, {
        "code": "MySQL",
        "text": "关系型数据库",
      }, {
        "code": "Redis",
        "text": "非关系型数据库",
      }, {
        "code": "ShowDoc",
        "text": "API 在线文档分享工具",
      }
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})