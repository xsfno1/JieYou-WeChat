import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 输入框内容
    value: "小米",
    // 最近搜索
    recentSearch: [],
    // 热门搜索
    isHotSearchHidden: false,
    hotSearch: [{
        value: "小米",
        hot: true
      },
      {
        value: "华为",
        hot: true
      },
      {
        value: "毛巾",
        hot: false
      },
      {
        value: "汽车坐垫",
        hot: false
      },
      {
        value: "厨房碗柜",
        hot: false
      },
      {
        value: "豆浆机",
        hot: false
      },
      {
        value: "罗汉果",
        hot: false
      },
      {
        value: "晾衣架",
        hot: false
      },
      {
        value: "剃须刀",
        hot: false
      },
    ],
  },

  /**
   * 清空最近搜索内容
   */
  handleClearRecent(e) {
    wx.setStorageSync("search", []);
    this.setData({
      recentSearch: []
    })
  },

  /**
   * 表单提交
   */
  formSubmit: function(e) {
    // 获取输入框内容
    const value = e.detail.value.input || this.data.value;

    // ################ 存入最近搜索缓存 Start 
    let search = wx.getStorageSync("search") || [];
    search.unshift(value);
    // 设置搜索缓存最大长度为10
    if (search.length > 10) {
      search.pop();
    }
    this.setData({
      recentSearch: search
    })
    wx.setStorageSync("search", search);
    // ################ 存入最近搜索缓存 End

    wx.navigateTo({
      url: `/pages/goods_list/goods_list?query=${value}`
    })
  },

  /**
   * 热门搜索显示/隐藏
   */
  handleHotSearchShow(e) {
    this.setData({
      isHotSearchHidden: !this.data.isHotSearchHidden
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // ################ 最近搜索 Start 
    let search = wx.getStorageSync("search") || [];
    if (search.length) {
      this.setData({
        recentSearch: search
      })
    }
    // ################ 最近搜索 Start 
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
})