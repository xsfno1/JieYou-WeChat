// pages/category/category.js

import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 分类左侧数据
    categoryLeftList: [],
    // 分类右侧数据
    categoryRightList: [],
    // 左侧当前状态索引
    currentIndex: 0,
    // 右侧菜单距离顶部距离
    scrollTop: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 获取分类数据 ######################### Start
    this.getLeftCategoryData();
    // 获取分类数据 ######################### End
  },

  onShow:function(){
    
  },


  /**
   * 监听用户下拉刷新
   */
  onPullDownRefresh: function () {
    this.onLoad();
    wx.stopPullDownRefresh();
  },

  /**
   * 获取左侧分类数据
   */
  async getLeftCategoryData() {
    const res = await request({
      url: "/categories/parent"
    });
    this.Cates = res.data;
    this.setData({
      categoryLeftList: res.data
    })
    // 同时获取右侧分类数据：默认获取第一个节点的子分类数据
    this.getRightCategoryData(this.data.categoryLeftList[0]["catId"]);
  },
  /**
   * 获取右侧分类数据
   * @param pid 父节点ID
   */
  async getRightCategoryData(pid) {
    const res = await request({
      url: `/categories/pid/${pid}`
    });
    this.setData({
      categoryRightList: res.data
    })
  },


  /**
   * 左侧分类菜单监听点击事件
   */
  handleLeftNavTap(event) {
    // index :　选择下标
    // id    :  选择分类ID
    const {
      index,
      id
    } = event.target.dataset;
    // 获取相应右侧分类数据
    this.getRightCategoryData(id);
    
    this.setData({
      currentIndex: index,
      scrollTop: 0
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
})