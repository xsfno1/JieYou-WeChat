import {
  request
} from "../../request/index.js";
import {
  showToast,
  chooseImage
} from "../../utils/asyncWx.js";
import regeneratorRuntime from '../../lib/runtime/runtime.js';
Page({

  data: {
    // 文本框内容
    value: '',
    // Tabs
    tabs: [{
        id: 0,
        value: "体验问题",
        isActive: true
      },
      {
        id: 1,
        value: "商品、商家投诉",
        isActive: false
      }
    ],
    // 上传的图片集合
    imgList: [],
    // 问题种类
    problemTypes: [{
        id: 0,
        name: "功能建议",
        isActive: false
      },
      {
        id: 1,
        name: "购买遇到的问题",
        isActive: false
      },
      {
        id: 2,
        name: "性能问题",
        isActive: false
      },
      {
        id: 3,
        name: "其他",
        isActive: false
      },
    ]
  },

  /**
   * Tabs 属性改变：子传父
   */
  handletabsItemChange(e) {
    const {
      index
    } = e.detail;
    // 改变Tabs下标
    let tabs = this.data.tabs;
    tabs.forEach(v => v.isActive = v.id === index ? true : false);
    this.setData({
      tabs
    });
  },

  /**
   * 上传图片按钮监听事件
   */
  async handleUploadImg(e) {
    try {
      // 打开微信内置选择图片API
      const res = await chooseImage();
      // 数据合并
      let imgList = [...this.data.imgList, ...res.tempFilePaths];
      this.setData({
        imgList
      })
      // 成功提示
      await showToast({
        title: "上传图片成功"
      })
    } catch (err) {
      // 失败提示
      console.error(err);
      await showToast({
        title: "上传图片失败",
        icon: "none"
      })
    }
  },

  /**
   * 图片删除
   */
  handleDeleImg(e) {
    // 1 获取删除图片的下标
    const {
      index
    } = e.currentTarget.dataset;
    // 2 获取图片列表
    let {
      imgList
    } = this.data;
    // 3 删除
    imgList.splice(index, 1);
    // 4 重新返回 Data 中
    this.setData({
      imgList
    })
  },

  /**
   * 问题种类状态改变：选中/不选中
   */
  handleProTypesStatus(e) {
    const {
      id
    } = e.currentTarget.dataset;
    let problemTypes = this.data.problemTypes;
    problemTypes[id].isActive = !problemTypes[id].isActive
    this.setData({
      problemTypes
    })
  },

  /**
   * 监听TextArea文本框失去焦点：获取TextArea的值内容
   */
  bindTextAreaBlur: function(e) {
    this.setData({
      value: e.detail.value
    })
  },

  /**
   * 表单提交
   */
  async handleSubmit(e) {
    const value = this.data.value || "";
    if (value.length === 0) {
      await showToast({
        title: "内容不得为空",
        icon: "none"
      })
      return;
    }
    await showToast({
      title: "反馈提交成功"
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
     * 用户点击右上角分享
     */
  onShareAppMessage: function () {

  },
})