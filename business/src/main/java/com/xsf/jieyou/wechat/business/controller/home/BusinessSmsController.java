package com.xsf.jieyou.wechat.business.controller.home;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.xsf.jieyou.wechat.business.dto.SmsParam;
import com.xsf.jieyou.wechat.business.exception.BusinessException;
import com.xsf.jieyou.wechat.business.redis.service.RedisSmsService;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.commons.utils.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * SMS短信服务
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/7 0007 0:59
 * @see com.xsf.jieyou.wechat.business.controller.home
 **/
@Slf4j
@RestController
@RequestMapping("/wechat/sms")
public class BusinessSmsController {
    /**
     * 短信签名名称
     */
    private static final String SIGN_NAME = "解忧杂货店";
    /**
     * 是否开启SMS服务(目的：SAVE MONEY)
     */
    @Value("${IS_OPEN_SMS}")
    private String isOpenSms;

    @Resource
    private RedisSmsService redisSmsService;

    /**
     * 注册账号短信验证发送
     * {
     * "Message":"OK",
     * "RequestId":"2184201F-BFB3-446B-B1F2-C746B7BF0657",
     * "BizId":"197703245997295588^0",
     * "Code":"OK"
     * }
     *
     * @param phone 手机号码
     * @return https://help.aliyun.com/document_detail/101346.html?spm=a2c4g.11186623.2.14.238d56e0o00RO4
     */
    @GetMapping("/order/{phone}")
    public ResponseResult<SmsParam> send(@PathVariable String phone) throws Exception {
        // ONLY FOR SAVE MONEY...
        if (StringUtils.isEmpty(this.isOpenSms) || "false".equals(this.isOpenSms)) {
            return new ResponseResult<>(ResponseStatus.FAIL.code(),
                    phone + " : ALREADY CLOSE SMS SERVICE...PLEASE CALL XSF...");
        }

        // 短信模板ID
        String templateCode = "SMS_175540285";
        // 随机验证码
        String code = RandomStringUtils.randomNumeric(4);
        // 将 验证码 存入Redis缓存中
        Integer flag = this.redisSmsService.insertRegisterCodeToRedis(phone, code);
        if (flag != 200) {
            throw new BusinessException(ResponseStatus.SEND_SMS_ERROR);
        }
        String templateParam = "{\"code\":\"" + code + "\"}";
        // Send
        SmsParam result = MapperUtils.json2pojo(this.baseSendSms(phone, templateCode, templateParam), SmsParam.class);
        return SmsParam.SMS_MESSAGE_SUCCESS.equals(result.getMessage()) ?
                new ResponseResult<>(ResponseStatus.OK.code(), result) :
                new ResponseResult<>(ResponseStatus.FAIL.code(), result);
    }

    /**
     * 发送短信服务方法
     *
     * @param phoneNumbers  接收短信的手机号码
     * @param templateCode  短信模板ID
     * @param templateParam 短信模板变量对应的实际值，JSON格式
     * @return {"Message":"OK","RequestId":"2184201F-BFB3-446B-B1F2-C746B7BF0657","BizId":"197703245997295588^0",
     * "Code":"OK"}
     */
    private String baseSendSms(String phoneNumbers, String templateCode, String templateParam) throws Exception {
        DefaultProfile profile = DefaultProfile.getProfile(
                "cn-hangzhou",
                "LTAI4FcQuPS9bQch4S5mTV6P",
                "Wmu7NhtskIDmeWr8wYVMgNsLOm26vX");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        // 接收短信的手机号码。
        request.putQueryParameter("PhoneNumbers", phoneNumbers);
        // 短信签名名称。请在控制台签名管理页面签名名称一列查看。
        request.putQueryParameter("SignName", SIGN_NAME);
        // 短信模板ID。请在控制台模板管理页面模板CODE一列查看。示范：SMS_153055065
        request.putQueryParameter("TemplateCode", templateCode);
        // 短信模板变量对应的实际值，JSON格式。示范：{"code":"1111"}
        request.putQueryParameter("TemplateParam", templateParam);

        CommonResponse response = client.getCommonResponse(request);
        return response.getData();
    }
}
