package com.xsf.jieyou.wechat.business.controller.home;

import com.xsf.jieyou.wechat.business.redis.service.RedisHomeService;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.provider.api.ProviderTbIndexCateService;
import com.xsf.jieyou.wechat.provider.api.ProviderTbIndexFloorService;
import com.xsf.jieyou.wechat.provider.api.ProviderTbIndexSwiperService;
import com.xsf.jieyou.wechat.provider.domain.TbIndexCate;
import com.xsf.jieyou.wechat.provider.domain.TbIndexSwiper;
import com.xsf.jieyou.wechat.provider.dto.FloorDataDto;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 主页控制层。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/30 0030 0:05
 * @see com.xsf.jieyou.wechat.business.controller.home
 **/

@RestController
@RequestMapping("/wechat/home")
public class BusinessHomeController {
    @Reference(version = "1.0.0")
    private ProviderTbIndexSwiperService providerTbIndexSwiperService;
    @Reference(version = "1.0.0")
    private ProviderTbIndexCateService providerTbIndexCateService;
    @Reference(version = "1.0.0")
    private ProviderTbIndexFloorService providerTbIndexFloorService;

    @Resource
    private RedisHomeService redisTbHomeService;

    /**
     * 查询主页的轮播图数据
     *
     * @return {@link ResponseResult}
     */
    @GetMapping("/swiperdata")
    public ResponseResult<List<TbIndexSwiper>> selectAllSwiperData() {
        List<TbIndexSwiper> swiperList = this.providerTbIndexSwiperService.selectAll();
        return new ResponseResult<>(ResponseStatus.OK.code(), swiperList);
    }

    /**
     * 查询主页的导航菜单
     *
     * @return {@link ResponseResult}
     */
    @GetMapping("/catitems")
    public ResponseResult<List<TbIndexCate>> cateItems() {
        List<TbIndexCate> list = this.providerTbIndexCateService.selectCateItems();
        return new ResponseResult<>(ResponseStatus.OK.code(), list);
    }

    /**
     * 查询主页的楼层商品数据
     *
     * @return {@link ResponseResult}
     */
    @GetMapping("/floordata")
    public ResponseResult<List<FloorDataDto>> floorData() {
        List<FloorDataDto> list = this.providerTbIndexFloorService.selectHomeFloorData();
        return new ResponseResult<>(ResponseStatus.OK.code(), list);
    }
}
