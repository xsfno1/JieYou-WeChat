package com.xsf.jieyou.wechat.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 短信返回模型。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 8:56
 * @see com.xsf.jieyou.wechat.business.dto
 **/
@Data
public class SmsParam implements Serializable {
    /**
     * 发送短信成功返回信息   Message:OK
     */
    public static final String SMS_MESSAGE_SUCCESS = "OK";
    /**
     * 错误信息
     */
    @JsonProperty("Message")
    private String message;
    @JsonProperty("RequestId")
    private String requestId;
    @JsonProperty("BizId")
    private String bizId;
    /**
     * 错误码
     */
    @JsonProperty("Code")
    private String code;
}
