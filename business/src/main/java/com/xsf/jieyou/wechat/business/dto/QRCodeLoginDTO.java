package com.xsf.jieyou.wechat.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 二维码登录DTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QRCodeLoginDTO implements Serializable {
    private static final long serialVersionUID = 6060623348453068725L;
    /**
     * 二维码状态码
     */
    private String code;
    /**
     * 信息
     */
    private String msg;
    /**
     *
     */
}
