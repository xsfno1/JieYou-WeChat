package com.xsf.jieyou.wechat.business.redis.service;

import com.xsf.jieyou.wechat.provider.dto.CateNodeDTO;

import java.util.List;

/**
 * Redis：分类
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/7 0007 5:44
 * @see com.xsf.jieyou.wechat.business.redis.service
 **/
public interface RedisCategoryService {
    /**
     * 最上级父节点（不递归）：插入
     *
     * @param list {@link List<CateNodeDTO>} 对象
     */
    void insertRedisCateParent(List<CateNodeDTO> list);

    /**
     * 最上级父节点（不递归）：查询
     *
     * @return {@link List<CateNodeDTO>}
     */
    List<CateNodeDTO> selectRedisCateParent();

    /**
     * 分类子节点（递归）：插入
     *
     * @param list {@link List<CateNodeDTO>} 对象
     * @param pid  {@link Integer}
     */
    void insertRedisCateByPid(List<CateNodeDTO> list, Integer pid);

    /**
     * 分类子节点（不递归）：查询
     *
     * @param pid {@link Integer}
     * @return {@link List<CateNodeDTO>}
     */
    List<CateNodeDTO> selectRedisCateByPid(Integer pid);
}
