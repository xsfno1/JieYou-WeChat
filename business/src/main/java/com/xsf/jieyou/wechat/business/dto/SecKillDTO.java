package com.xsf.jieyou.wechat.business.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 20:17
 * @see com.xsf.jieyou.wechat.business.dto
 **/
@Data
public class SecKillDTO implements Serializable {

    private static final long serialVersionUID = -5738871762778148362L;
    /**
     * 商品ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品图片
     */
    private String goodsSmallLogo;

    /**
     * 秒杀价格
     */
    private Integer newPrice;

    private Integer oldPrice;

    /**
     * 秒杀商品容量
     */
    private Integer seckillReserve;

    /**
     * 已买数量
     */
    private Integer boughtCount;

    /**
     * 已买百分比
     */
    private Integer boughtPercent;

    /**
     * 场次
     */
    private Integer seckillDate;

}
