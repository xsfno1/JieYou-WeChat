package com.xsf.jieyou.wechat.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/29 0029 22:52
 * @see com.xsf.jieyou.wechat.business
 **/
@SpringBootApplication
public class BusinessWeChatApplication {
    public static void main(String[] args) {
        SpringApplication.run(BusinessWeChatApplication.class, args);
    }
}
