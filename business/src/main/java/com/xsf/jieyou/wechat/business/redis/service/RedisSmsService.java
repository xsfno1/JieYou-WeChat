package com.xsf.jieyou.wechat.business.redis.service;

/**
 * Redis：SMS短信服务
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 8:18
 * @see com.xsf.jieyou.wechat.business.redis.service
 **/
public interface RedisSmsService {
    /**
     * 将 验证码 存入Redis缓存中
     *
     * @param phone 手机号码
     * @param code  验证码
     * @return {@link Integer} 成功 200
     */
    Integer insertRegisterCodeToRedis(String phone, String code);

    /**
     * 验证码：查询
     *
     * @param phone 手机号码
     * @return {@link String}
     */
    String selectRegisterCode(String phone);
}
