package com.xsf.jieyou.wechat.business.redis.service;

import com.xsf.jieyou.wechat.provider.domain.TbIndexCate;
import com.xsf.jieyou.wechat.provider.domain.TbIndexSwiper;
import com.xsf.jieyou.wechat.provider.dto.FloorDataDto;

import java.util.List;

/**
 * Redis：主页
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/7 0007 3:18
 * @see com.xsf.jieyou.wechat.business.redis.service
 **/
public interface RedisHomeService {
    /**
     * 轮播图：插入
     *
     * @param list 轮播图对象
     */
    void insertRedisSwiper(List<TbIndexSwiper> list);

    /**
     * 轮播图：查询
     *
     * @return {@link List<TbIndexSwiper>}
     */
    List<TbIndexSwiper> selectRedisSwiper();

    /**
     * 导航菜单：插入
     *
     * @param list 轮播图对象
     */
    void insertRedisNav(List<TbIndexCate> list);

    /**
     * 导航菜单：查询
     *
     * @return {@link List<TbIndexCate>}
     */
    List<TbIndexCate> selectRedisNav();

    /**
     * 楼层商品：插入
     *
     * @param list 轮播图对象
     */
    void insertRedisFloor(List<FloorDataDto> list);

    /**
     * 楼层商品：查询
     *
     * @return {@link List<FloorDataDto>}
     */
    List<FloorDataDto> selectRedisFloor();
}
