package com.xsf.jieyou.wechat.business.controller.home;

import com.google.common.collect.Lists;
import com.xsf.jieyou.wechat.business.redis.service.RedisCategoryService;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.provider.api.ProviderTbCategoryService;
import com.xsf.jieyou.wechat.provider.domain.TbCategory;
import com.xsf.jieyou.wechat.provider.dto.CateNodeDTO;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品分类
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 23:10
 * @see com.xsf.jieyou.wechat.business.controller.home
 **/
@RestController
@RequestMapping("/wechat/categories")
public class BusinessCategoryController {
    /**
     * 最上级父节点ID
     */
    private static final Integer PARENT_CATE_ID = 0;
    @Reference(version = "1.0.0")
    private ProviderTbCategoryService providerTbCategoryService;

    @Resource
    private RedisCategoryService redisCategoryService;

    /**
     * 根据父节点ID获取分类
     *
     * @param parentid 父节点ID
     * @return {@link ResponseResult<TbCategory>}
     */
    @GetMapping("/pid/{parentid}")
    public ResponseResult<List<CateNodeDTO>> selectCateByPid(@PathVariable Integer parentid) {
        // 1 查询Redis缓存，有：取缓存   无：查询数据库
        List<CateNodeDTO> list = null;
        try {
            list = this.redisCategoryService.selectRedisCateByPid(parentid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 2 查询数据库
        if (list == null) {
            System.out.println("Redis Cate：未发现缓存，现从数据库中拉取数据...");
            // 递归获取分类节点以及其子节点
            list = this.getCatList(parentid);

            // 3 存入Redis缓存
            try {
                if (list.size() != 0) {
                    this.redisCategoryService.insertRedisCateByPid(list, parentid);
                    System.out.println("Redis Cate：存入缓存成功...");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Redis Cate：发现缓存...");
        }

        return new ResponseResult<>(ResponseStatus.OK.code(), ResponseStatus.OK.message(), list);
    }

    /**
     * 获取最上级父节点（0）的直系子节点数据，不递归查询下下级节点数据
     *
     * @return {@link ResponseResult<TbCategory>}
     */
    @GetMapping("/parent")
    public ResponseResult<List<CateNodeDTO>> selectParentCate() {
        // 1 查询Redis缓存，有：取缓存   无：查询数据库
        try {
            List<CateNodeDTO> redisCate = this.redisCategoryService.selectRedisCateParent();
            if (redisCate != null && redisCate.size() != 0) {
                System.out.println("Redis Cate CateParent：发现缓存...");
                return new ResponseResult<>(ResponseStatus.OK.code(), redisCate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 2 查询数据库
        System.out.println("Redis Cate CateParent：未发现缓存，现从数据库中拉取数据...");
        List<TbCategory> tbCategoryList = this.providerTbCategoryService.selectCategoryByParentId(PARENT_CATE_ID);
        List<CateNodeDTO> list = Lists.newArrayList();
        tbCategoryList.forEach(tbCategory -> {
            CateNodeDTO catNode = new CateNodeDTO();
            BeanUtils.copyProperties(tbCategory, catNode);
            list.add(catNode);
        });

        // 3 存入Redis缓存
        try {
            if (list.size() != 0) {
                this.redisCategoryService.insertRedisCateParent(list);
                System.out.println("Redis Cate CateParent：存入缓存成功...");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseStatus.OK.code(), list);
    }


    /**
     * 递归获取分类节点以及其子节点
     *
     * @param parentId 父节点ID
     * @return {@link List<CateNodeDTO>}
     */
    private List<CateNodeDTO> getCatList(Integer parentId) {
        List<TbCategory> list = this.providerTbCategoryService.selectCategoryByParentId(parentId);

        List<CateNodeDTO> resultList = Lists.newLinkedList();
        for (TbCategory tbCategory : list) {
            CateNodeDTO catNode = new CateNodeDTO();
            BeanUtils.copyProperties(tbCategory, catNode);
            // 1 判断是否为父节点
            if (tbCategory.getIsParent()) {
                // 是，递归查询子叶子节点
                catNode.setChildren(getCatList(tbCategory.getCatId()));
            }
            resultList.add(catNode);
        }
        return resultList;
    }
}
