package com.xsf.jieyou.wechat.business.redis.service;

import com.xsf.jieyou.wechat.provider.dto.GoodsDetailDTO;

/**
 * Redis：商品
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/7 0007 5:32
 * @see com.xsf.jieyou.wechat.business.redis.service
 **/
public interface RedisGoodsService {
    /**
     * 商品详情：插入
     *
     * @param goodsDetailDTO {@link GoodsDetailDTO} 对象
     * @param goodsId        {@link Integer}
     */
    void insertRedisGoodsDetail(GoodsDetailDTO goodsDetailDTO, Integer goodsId);

    /**
     * 商品详情：查询
     *
     * @param goodsId {@link Integer}
     * @return {@link GoodsDetailDTO}
     */
    GoodsDetailDTO selectRedisGoodsDetail(Integer goodsId);
}
