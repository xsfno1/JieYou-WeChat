package com.xsf.jieyou.wechat.business.redis.service.impl;

import com.xsf.jieyou.wechat.business.redis.service.RedisQrCodeService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 二维码
 *
 * @author xsfno1
 * @version v1.0.0
 * @date 2020/3/10 22:47
 * @see com.xsf.jieyou.wechat.business.redis.service.impl
 **/
@Service
public class RedisQrCodeServiceImpl implements RedisQrCodeService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 基础 Redis KEY
     */
    private static final String BASE_REDIS_KEY = "REDIS:KEY:JIEYOU:WECHAT:QRCODE";
    /**
     * 过期时间
     */
    private static final Integer REDIS_KEY_EXPIRE_SECOND = 10;

    /**
     * 存
     *
     * @param code  {@link String} 唯一标识
     * @param state {@link String} 二维码状态,-1 授权失败 0 未扫描 1 扫描成功 2 授权成功  null 过期
     * @return {@link Integer}
     */
    @Override
    public Integer insertRedisQrCode(String code, String state) {
        String key = BASE_REDIS_KEY + ":" + code;
        this.redisTemplate.opsForValue().set(key, code, REDIS_KEY_EXPIRE_SECOND, TimeUnit.SECONDS);
        return 200;
    }

    /**
     * 取
     *
     * @param code {@link String} 唯一标识
     * @return {@link String} 二维码状态,-1 授权失败 0 未扫描 1 扫描成功 2 授权成功  null 过期
     */
    @Override
    public String selectRedisQrCode(String code) {
        String key = BASE_REDIS_KEY + ":" + code;
        return (String) this.redisTemplate.opsForValue().get(key);
    }
}
