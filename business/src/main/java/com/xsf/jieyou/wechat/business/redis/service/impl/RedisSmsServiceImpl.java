package com.xsf.jieyou.wechat.business.redis.service.impl;

import com.xsf.jieyou.wechat.business.redis.service.RedisSmsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 8:19
 * @see com.xsf.jieyou.wechat.business.redis.service.impl
 **/
@Service
public class RedisSmsServiceImpl implements RedisSmsService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 验证码 RedisKey
     */
    @Value("${REDIS_KEY_CODE}")
    private String redisKeyCode;
    /**
     * RedisKey 过期时间
     */
    @Value("${REDIS_KEY_CODE_EXPIRE_SECOND}")
    private Integer redisKeyExpireSecond;

    /**
     * 将 验证码 存入Redis缓存中
     *
     * @param phone 手机号码
     * @param code  验证码
     * @return {@link Integer} 成功 200
     */
    @Override
    public Integer insertRegisterCodeToRedis(String phone, String code) {
        String key = this.redisKeyCode + ":" + phone;
        this.redisTemplate.opsForValue().set(key, code, this.redisKeyExpireSecond,
                TimeUnit.SECONDS);
        return 200;
    }

    /**
     * 验证码：查询
     *
     * @param phone 手机号码
     * @return {@link String}
     */
    @Override
    public String selectRegisterCode(String phone) {
        String key = this.redisKeyCode + ":" + phone;
        return (String) this.redisTemplate.opsForValue().get(key);
    }
}
