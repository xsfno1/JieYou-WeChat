package com.xsf.jieyou.wechat.business.redis.service.impl;

import com.xsf.jieyou.wechat.business.redis.service.RedisGoodsService;
import com.xsf.jieyou.wechat.provider.dto.GoodsDetailDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Redis：商品
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/7 0007 5:35
 * @see com.xsf.jieyou.wechat.business.redis.service.impl
 **/
@Service
public class RedisGoodsServiceImpl implements RedisGoodsService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 商品详情 RedisKey
     */
    @Value("${REDIS_KEY_GOODS_DETAIL}")
    private String redisKeyGoodsDetail;

    /**
     * RedisKey 过期时间
     */
    @Value("${REDIS_KEY_EXPIRE_SECOND}")
    private Integer redisKeyExpireSecond;

    /**
     * 商品详情：插入
     *
     * @param goodsDetailDTO {@link GoodsDetailDTO} 对象
     * @param goodsId        {@link Integer}
     */
    @Override
    public void insertRedisGoodsDetail(GoodsDetailDTO goodsDetailDTO, Integer goodsId) {
        String key = this.redisKeyGoodsDetail + ":" + goodsId;
        this.redisTemplate.opsForValue().set(key, goodsDetailDTO, this.redisKeyExpireSecond, TimeUnit.SECONDS);
    }

    /**
     * 商品详情：查询
     *
     * @param goodsId {@link Integer}
     * @return {@link GoodsDetailDTO}
     */
    @Override
    public GoodsDetailDTO selectRedisGoodsDetail(Integer goodsId) {
        String key = this.redisKeyGoodsDetail + ":" + goodsId;
        return (GoodsDetailDTO) this.redisTemplate.opsForValue().get(key);
    }
}
