package com.xsf.jieyou.wechat.business.mao.controller;

import com.xsf.jieyou.wechat.business.redis.service.RedisSmsService;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 正则猫的接口
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 8:33
 * @see com.xsf.jieyou.wechat.business.mao.controller
 **/
@RestController
@RequestMapping("/wechat/mao")
public class MaoController {
    @Resource
    private RedisSmsService redisSmsService;

    /**
     * 登录
     *
     * @param phone 手机号码
     * @param code  验证码
     * @return {@link ResponseResult}
     */
    @GetMapping("/login/{phone}/{code}")
    public ResponseResult<?> login(@PathVariable String phone, @PathVariable String code) {
        // 1 获取缓存中的验证码
        String redisCode = this.redisSmsService.selectRegisterCode(phone);
        // 2 校验
        return code.equals(redisCode) ? ResponseResult.ok() : ResponseResult.fail();
    }
}
