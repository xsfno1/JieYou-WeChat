package com.xsf.jieyou.wechat.business.exception;

import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;

/**
 * 全局业务异常
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 8:28
 * @see com.xsf.jieyou.wechat.business.exception
 **/
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = -4235422592743789819L;

    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BusinessException() {
    }

    public BusinessException(ResponseStatus status) {
        super(status.message());
        this.code = status.code();
    }
}
