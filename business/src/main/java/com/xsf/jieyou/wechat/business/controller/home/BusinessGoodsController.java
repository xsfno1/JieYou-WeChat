package com.xsf.jieyou.wechat.business.controller.home;

import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.commons.entity.PageResult;
import com.xsf.jieyou.wechat.provider.api.ProviderTbGoodsService;
import com.xsf.jieyou.wechat.provider.domain.TbGoods;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 23:10
 * @see com.xsf.jieyou.wechat.business.controller.home
 **/
@RestController
@RequestMapping("/wechat/goods")
public class BusinessGoodsController {
    @Reference(version = "1.0.0")
    private ProviderTbGoodsService providerTbGoodsService;

    /**
     * @param query    关键字
     * @param cid      分类id
     * @param orderby  排序字段
     * @param pagenum  页码，默认1
     * @param pagesize 页容量，默认20
     * @return {@link ResponseResult<PageResult>}
     */
    @GetMapping("/search")
    public ResponseResult<PageResult<TbGoods>> search(
            @RequestParam(required = false) String query,
            @RequestParam(required = false) Integer cid,
            @RequestParam(required = false) String orderby,
            @RequestParam(defaultValue = "1") Integer pagenum,
            @RequestParam(defaultValue = "20") Integer pagesize) {
        PageResult<TbGoods> pageResult = this.providerTbGoodsService.searchGoods(query, cid, pagenum, pagesize,
                orderby);
        return new ResponseResult<>(ResponseStatus.OK.code(), pageResult);
    }
}
