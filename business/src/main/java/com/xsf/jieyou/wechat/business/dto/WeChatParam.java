package com.xsf.jieyou.wechat.business.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 微信小程序-用户信息  参数类
 *
 * @author xsfno1
 * @version v1.0.0
 * @date 2020/3/12 20:16
 * @see com.xsf.jieyou.wechat.business.dto
 **/
@Data
public class WeChatParam implements Serializable {
    private static final long serialVersionUID = -590391147954579241L;

    private String nickName;
    private Integer gender;
    private String language;
    private String city;
    private String province;
    private String country;
    private String avatarUrl;
}
