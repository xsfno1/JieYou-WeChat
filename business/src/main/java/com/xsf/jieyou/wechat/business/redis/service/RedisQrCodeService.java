package com.xsf.jieyou.wechat.business.redis.service;

/**
 * 二维码
 *
 * @author xsfno1
 * @version v1.0.0
 * @date 2020/3/10 22:43
 * @see com.xsf.jieyou.wechat.business.redis.service
 **/
public interface RedisQrCodeService {
    /**
     * 存
     *
     * @param code  {@link String} 唯一标识
     * @param state {@link String} 二维码状态,-1 授权失败 0 未扫描 1 扫描成功 2 授权成功  null 过期
     * @return {@link Integer}
     */
    Integer insertRedisQrCode(String code, String state);

    /**
     * 取
     *
     * @param code {@link String} 唯一标识
     * @return {@link String} 二维码状态,-1 授权失败 0 未扫描 1 扫描成功 2 授权成功  null 过期
     */
    String selectRedisQrCode(String code);
}
