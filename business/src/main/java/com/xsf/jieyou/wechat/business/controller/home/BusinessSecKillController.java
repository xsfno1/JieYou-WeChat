package com.xsf.jieyou.wechat.business.controller.home;

import com.google.common.collect.Lists;
import com.xsf.jieyou.wechat.business.dto.SecKillDTO;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.provider.api.ProviderTbSecKillService;
import com.xsf.jieyou.wechat.provider.domain.TbSeckill;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 秒杀
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 19:02
 * @see com.xsf.jieyou.wechat.business.controller.home
 **/
@RestController
@RequestMapping("/wechat/seckill")
public class BusinessSecKillController {
    @Reference(version = "1.0.0")
    private ProviderTbSecKillService providerTbSecKillService;

    @GetMapping("/all/{data}")
    public ResponseResult<List<SecKillDTO>> all(@PathVariable Integer data) {
        // 1.获取数据
        List<TbSeckill> oldList = this.providerTbSecKillService.selectSecKillList(data);

        // 2.数据封装
        List<SecKillDTO> list = Lists.newArrayList();
        oldList.forEach(tbSeckill -> {
            SecKillDTO secKillDTO = new SecKillDTO();
            BeanUtils.copyProperties(tbSeckill, secKillDTO);
            if (secKillDTO.getSeckillReserve() != 0) {
                double percent = (double) secKillDTO.getBoughtCount() / secKillDTO.getSeckillReserve() * 100;
                if (percent < 1 && percent > 0) {
                    percent = 1;
                } else {
                    percent = Math.floor(percent);
                }
                secKillDTO.setBoughtPercent((int) percent);
                list.add(secKillDTO);
            }
        });

        return new ResponseResult<>(ResponseStatus.OK.code(), list);
    }

    @GetMapping("/limit")
    public ResponseResult<TbSeckill> currentLimiting() {
        TbSeckill tbSeckill = this.providerTbSecKillService.currentLimiting();
        if (tbSeckill == null) {
            // 触发限流
            return new ResponseResult<>(ResponseStatus.CURRENT_LIMITING.code(),
                    ResponseStatus.CURRENT_LIMITING.message());
        }
        return new ResponseResult<>(ResponseStatus.OK.code(),ResponseStatus.OK.message());
    }
}
