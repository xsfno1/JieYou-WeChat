package com.xsf.jieyou.wechat.business.controller.home;

import com.xsf.jieyou.wechat.business.redis.service.RedisGoodsService;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.provider.api.ProviderTbGoodsDetailService;
import com.xsf.jieyou.wechat.provider.dto.GoodsDetailDTO;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 商品详情
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 21:27
 * @see com.xsf.jieyou.wechat.business.controller.home
 **/
@RestController
@RequestMapping("/wechat/goods/detail")
public class BusinessGoodsDetailController {
    @Reference(version = "1.0.0")
    private ProviderTbGoodsDetailService providerTbGoodsDetailService;

    @Resource
    private RedisGoodsService redisGoodsService;

    /**
     * 查询商品详情，By商品主键
     *
     * @param goodsId 商品ID
     * @return {@link ResponseResult<GoodsDetailDTO>}
     */
    @GetMapping("/{goodsId}")
    public ResponseResult<GoodsDetailDTO> selectGoodsDetailByGid(@PathVariable Integer goodsId) {
        // 1 查询Redis缓存，有：取缓存   无：查询数据库
        GoodsDetailDTO goodsDetailDTO = null;
        try {
            goodsDetailDTO = this.redisGoodsService.selectRedisGoodsDetail(goodsId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 2 查询数据库
        if (goodsDetailDTO == null) {
            System.out.println("Redis Home Swiper：未发现缓存，现从数据库中拉取数据...");
            goodsDetailDTO = this.providerTbGoodsDetailService.selectGoodsDetailByGid(goodsId);

            // 3 存入Redis缓存
            try {
                if (goodsDetailDTO != null) {
                    this.redisGoodsService.insertRedisGoodsDetail(goodsDetailDTO, goodsId);
                    System.out.println("Redis Home Swiper：存入缓存成功...");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Redis Home Swiper：发现缓存...");
        }

        return new ResponseResult<>(ResponseStatus.OK.code(), goodsDetailDTO);
    }
}
