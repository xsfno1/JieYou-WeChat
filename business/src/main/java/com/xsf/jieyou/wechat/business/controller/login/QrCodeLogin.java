package com.xsf.jieyou.wechat.business.controller.login;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.google.common.collect.Maps;
import com.xsf.jieyou.wechat.business.dto.WeChatParam;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.commons.utils.MapperUtils;
import com.xsf.jieyou.wechat.commons.utils.OkHttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 扫码登录控制层 - 配合JieYou网页版
 * 在JieYou-WhChat中是不需要实现登录逻辑的,因为是直接用微信登录的
 * 本控制层存在原因:
 * 因为微信小程序指定域名,而JieYou和JieYou-WhChat不使用同一个服务器,所以需要JieYou-WhChat服务器转发JieYou服务器
 *
 * @author xsfno1
 * @version v1.0.0
 * @date 2020/3/13 15:00
 * @see com.xsf.jieyou.wechat.business.controller.login
 **/
@RestController
@RefreshScope
@RequestMapping("wechat/qrcode")
@Slf4j
public class QrCodeLogin {
    @Value("${qrcode.url.scan}")
    private String qrCodeUrlScan;
    @Value("${qrcode.url.author}")
    private String qrCodeUrlAuthor;

    /**
     * 二维码扫描
     */
    @GetMapping("scan")
    public ResponseResult scan(String code) {
        System.out.println("二维码扫描 CODE:" + code);
        try {
            String url = this.qrCodeUrlScan + code;
            ResponseBody responseBody = OkHttpClientUtil.getInstance().getData(url).body();
            if (responseBody != null) {
                String jsonStr = responseBody.string();
                System.out.println(jsonStr);
                Map<String, Object> jsonMap = MapperUtils.json2map(jsonStr);
                if ((Boolean) jsonMap.get("success")) {
                    return new ResponseResult(ResponseStatus.SUCCESS);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult(ResponseStatus.FAIL);
    }

    /**
     * 授权登录
     */
    @PostMapping("author/{code}/{isAuthor}")
    public ResponseResult author(@PathVariable String code,
                                 @PathVariable Integer isAuthor,
                                 @RequestBody WeChatParam weChatParam) {
        try {
            String jsonParam = MapperUtils.obj2jsonIgnoreNull(weChatParam);

            String url = this.qrCodeUrlAuthor + code + "/" + isAuthor;
            String responseBody = OkHttpClientUtil.getInstance().postJson(url, jsonParam);

            assert responseBody != null;
            log.info(responseBody);
            return new ResponseResult(ResponseStatus.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult(ResponseStatus.FAIL);
    }
}
