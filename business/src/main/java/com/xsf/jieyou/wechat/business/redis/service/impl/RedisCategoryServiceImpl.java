package com.xsf.jieyou.wechat.business.redis.service.impl;

import com.xsf.jieyou.wechat.business.redis.service.RedisCategoryService;
import com.xsf.jieyou.wechat.provider.dto.CateNodeDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/7 0007 5:47
 * @see com.xsf.jieyou.wechat.business.redis.service.impl
 **/
@Service
public class RedisCategoryServiceImpl implements RedisCategoryService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Value("${REDIS_KEY_CATE_PARENT}")
    private String redisKeyCateParent;
    @Value("${REDIS_KEY_CATE}")
    private String redisKeyCate;

    /**
     * RedisKey 过期时间
     */
    @Value("${REDIS_KEY_EXPIRE_SECOND}")
    private Integer redisKeyExpireSecond;

    /**
     * 最上级父节点（不递归）：插入
     *
     * @param list {@link List<CateNodeDTO>} 对象
     */
    @Override
    public void insertRedisCateParent(List<CateNodeDTO> list) {
        this.redisTemplate.opsForValue().set(this.redisKeyCateParent, list, this.redisKeyExpireSecond,
                TimeUnit.SECONDS);
    }

    /**
     * 最上级父节点（不递归）：查询
     *
     * @return {@link List<CateNodeDTO>}
     */
    @Override
    public List<CateNodeDTO> selectRedisCateParent() {
        return (List<CateNodeDTO>) this.redisTemplate.opsForValue().get(this.redisKeyCateParent);
    }

    /**
     * 分类子节点（递归）：插入
     *
     * @param list {@link List<CateNodeDTO>} 对象
     * @param pid  {@link Integer}
     */
    @Override
    public void insertRedisCateByPid(List<CateNodeDTO> list, Integer pid) {
        String key = this.redisKeyCate + ":" + pid;
        this.redisTemplate.opsForValue().set(key, list, this.redisKeyExpireSecond,
                TimeUnit.SECONDS);
    }

    /**
     * 分类子节点（不递归）：查询
     *
     * @param pid {@link Integer}
     * @return {@link List<CateNodeDTO>}
     */
    @Override
    public List<CateNodeDTO> selectRedisCateByPid(Integer pid) {
        String key = this.redisKeyCate + ":" + pid;
        return (List<CateNodeDTO>) this.redisTemplate.opsForValue().get(key);
    }
}
