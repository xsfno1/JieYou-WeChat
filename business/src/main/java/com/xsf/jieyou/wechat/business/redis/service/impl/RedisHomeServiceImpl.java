package com.xsf.jieyou.wechat.business.redis.service.impl;

import com.xsf.jieyou.wechat.business.redis.service.RedisHomeService;
import com.xsf.jieyou.wechat.provider.domain.TbIndexCate;
import com.xsf.jieyou.wechat.provider.domain.TbIndexSwiper;
import com.xsf.jieyou.wechat.provider.dto.FloorDataDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Redis：主页
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/7 0007 3:36
 * @see com.xsf.jieyou.wechat.business.redis.service.impl
 **/
@Service
public class RedisHomeServiceImpl implements RedisHomeService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 轮播图 RedisKey
     */
    @Value("${REDIS_KEY_HOME_SWIPER}")
    private String redisKeyHomeSwiper;
    /**
     * 导航菜单 RedisKey
     */
    @Value("${REDIS_KEY_HOME_NAV}")
    private String redisKeyHomeNav;
    /**
     * 楼层商品 RedisKey
     */
    @Value("${REDIS_KEY_HOME_FLOOR}")
    private String redisKeyHomeFloor;
    /**
     * RedisKey 过期时间
     */
    @Value("${REDIS_KEY_EXPIRE_SECOND}")
    private Integer redisKeyExpireSecond;

    /**
     * 轮播图：插入
     *
     * @param list 轮播图对象
     */
    @Override
    public void insertRedisSwiper(List<TbIndexSwiper> list) {
        this.redisTemplate.opsForValue().set(this.redisKeyHomeSwiper, list, this.redisKeyExpireSecond,
                TimeUnit.SECONDS);
    }

    /**
     * 轮播图：查询
     *
     * @return {@link List<TbIndexSwiper>}
     */
    @Override
    public List<TbIndexSwiper> selectRedisSwiper() {
        return (List<TbIndexSwiper>) this.redisTemplate.opsForValue().get(this.redisKeyHomeSwiper);
    }
    /**
     * 导航菜单：插入
     *
     * @param list 轮播图对象
     */
    @Override
    public void insertRedisNav(List<TbIndexCate> list) {
        this.redisTemplate.opsForValue().set(this.redisKeyHomeNav, list, this.redisKeyExpireSecond,
                TimeUnit.SECONDS);
    }

    /**
     * 导航菜单：查询
     *
     * @return {@link List<TbIndexCate>}
     */
    @Override
    public List<TbIndexCate> selectRedisNav() {
        return (List<TbIndexCate>) this.redisTemplate.opsForValue().get(this.redisKeyHomeNav);
    }
    /**
     * 楼层商品：插入
     *
     * @param list 轮播图对象
     */
    @Override
    public void insertRedisFloor(List<FloorDataDto> list) {
        this.redisTemplate.opsForValue().set(this.redisKeyHomeFloor, list, this.redisKeyExpireSecond,
                TimeUnit.SECONDS);
    }

    /**
     * 楼层商品：查询
     *
     * @return {@link List<FloorDataDto>}
     */
    @Override
    public List<FloorDataDto> selectRedisFloor() {
        return (List<FloorDataDto>) this.redisTemplate.opsForValue().get(this.redisKeyHomeFloor);
    }


}
