package com.xsf.jieyou.wechat.business.huang.controller;

import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.provider.domain.TIndeximg;
import com.xsf.jieyou.wechat.provider.domain.TMusic;
import com.xsf.jieyou.wechat.provider.huang.api.TIndeximgService;
import com.xsf.jieyou.wechat.provider.huang.api.TMusicService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 老黄接口。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 7:56
 * @see com.xsf.jieyou.wechat.business.huang.controller
 **/
@RestController
@RequestMapping("/wechat/xsh")
public class IndexController {
    @Reference(version = "1.0.0")
    private TIndeximgService tIndeximgService;
    @Reference(version = "1.0.0")
    private TMusicService tMusicService;

    @GetMapping(value = "/selImg")
    public ResponseResult<?> selImg() {
        List<TIndeximg> list = this.tIndeximgService.selectAll();
        return ResponseResult.ok(list);
    }

    @GetMapping(value = "/selmus")
    public ResponseResult<?> selmus() {
        List<TMusic> list = this.tMusicService.selectAll();
        return ResponseResult.ok(list);
    }

    @GetMapping(value = "/selmusbymid")
    public ResponseResult<?> selmusbymid(Integer mid) {
        TMusic tMusic = this.tMusicService.selmusBymid(mid);
        return ResponseResult.ok(tMusic);
    }
}
