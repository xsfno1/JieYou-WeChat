package com.xsf.jieyou.wechat.business.exception;

import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.AccessDeniedException;

/**
 * 全局业务异常处理
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/15 0015 17:28
 **/
@Slf4j
@ControllerAdvice
public class BusinessExceptionHandler {
    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handlerException(HttpServletRequest request, Exception ex) {
        ResponseResult<?> error = new ResponseResult<>();
        // 自定义业务异常
        if (ex instanceof BusinessException) {
            error.setCode(((BusinessException) ex).getCode());
            error.setMessage(ex.getMessage());
        }
        // 请求类型错误
        else if (ex instanceof HttpRequestMethodNotSupportedException) {
            error.setCode(ResponseStatus.METHOD_NOT_ALLOWED.code());
            error.setMessage(ResponseStatus.METHOD_NOT_ALLOWED.message());
        }
        // 传输的参数错误
        else if (ex instanceof HttpMessageNotReadableException || ex instanceof NullPointerException) {
            error.setCode(ResponseStatus.REQUEST_PARAM_INVALID.code());
            error.setMessage(ResponseStatus.REQUEST_PARAM_INVALID.message());
        }
        // 错误请求
        else if (ex instanceof IllegalArgumentException) {
            error.setCode(ResponseStatus.ILLEGAL_REQUEST.code());
            error.setMessage(ResponseStatus.ILLEGAL_REQUEST.message());
        }
        // 无访问权限
        else if (ex instanceof AccessDeniedException) {
            error.setCode(ResponseStatus.INSUFFICIENT_AUTHORITY.code());
            error.setMessage(ResponseStatus.INSUFFICIENT_AUTHORITY.message());
        }
        // 请求超时
        else if (ex instanceof IOException) {
            error.setCode(ResponseStatus.REQUEST_TIMEOUT.code());
            error.setMessage(ResponseStatus.REQUEST_TIMEOUT.message());
        }
        // 未知错误
        else {
            error.setCode(ResponseStatus.UNKNOWN.code());
            error.setMessage(ResponseStatus.UNKNOWN.message());
        }
        log.warn("\n[全局业务异常]\r\n异常处理类：{}\r\n异常编码：{}\n异常消息：{}\r\n异常记录：{}",
                this.getClass().getName(), error.getCode(), error.getMessage(), ex.toString());
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

}

