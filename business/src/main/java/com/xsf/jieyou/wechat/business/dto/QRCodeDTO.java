package com.xsf.jieyou.wechat.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 二维码DTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QRCodeDTO implements Serializable {

    private static final long serialVersionUID = 5139679285100946970L;
    /**
     * 随机生成码
     */
    private String code;
    /**
     * Base64 二维码文件
     */
    private String file;
}
