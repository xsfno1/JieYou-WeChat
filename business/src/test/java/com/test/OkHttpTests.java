package com.test;

import com.xsf.jieyou.wechat.business.dto.WeChatParam;
import com.xsf.jieyou.wechat.commons.dto.ResponseResult;
import com.xsf.jieyou.wechat.commons.dto.ResponseStatus;
import com.xsf.jieyou.wechat.commons.utils.BeanMapUtils;
import com.xsf.jieyou.wechat.commons.utils.MapperUtils;
import com.xsf.jieyou.wechat.commons.utils.OkHttpClientUtil;
import okhttp3.ResponseBody;
import org.junit.Test;
import org.springframework.cglib.beans.BeanMap;

import java.util.Map;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsfno1
 * @version v1.0.0
 * @date 2020/3/13 16:46
 * @see com.test
 **/
public class OkHttpTests {
    @Test
    public void test1() throws Exception{
        String url = "http://xsf.free.idcfengye.com/api/user/qrcode/scan?code=1238451291747815424";
        ResponseBody responseBody = OkHttpClientUtil.getInstance().getData(url).body();
        if (responseBody != null) {
            String jsonStr = responseBody.string();
            Map<String, Object> jsonMap = MapperUtils.json2map(jsonStr);
            System.out.println(jsonStr);
            if ((Boolean) jsonMap.get("success")) {
                System.out.println("SUCCESS");
            }else{
                System.out.println("FAIL");
            }
        }
    }

    @Test
    public void test2() throws Exception{
        WeChatParam weChatParam = new WeChatParam();
        weChatParam.setAvatarUrl("111");
        weChatParam.setGender(1);
        System.out.println(BeanMapUtils.beanToMap(weChatParam));
    }

}
