# 解忧杂货店 - WeChat


**解忧杂货店 - WeChat** 是一个微信小程序端B2C电商项目。本项目完成了电商项目的基本功能，通过 Python爬虫 技术爬取项目数据，用户可以在此系统中实现搜索商品、商品秒杀、查看商品详情、将商品加入购物车、购买商品、生成订单、完成购物等一系列操作。

## 开发工具

工具 | 说明 | 地址 
----|----|----
Intellij IDEA | 集成开发工具 | https://www.jetbrains.com/idea/download 
微信小程序开发工具 | 集成开发工具 | https://developers.weixin.qq.com/miniprogram/dev/devtools/devtools.html
FinalShell | 远程服务器连接工具 | http://www.hostbuf.com/ 
Postman | API/接口功能测试工具 | https://www.getpostman.com/downloads/ 
Navicat for MySQL | MySQL工具 | https://navicatformysql.en.softonic.com/


## 商业组件

服务 | 说明 | 地址
----|----|----
Aliyun SMS | 阿里云短信服务 | [官网](https://help.aliyun.com/product/44282.html?spm=a2c4g.11186623.6.540.5ba42e79CcfxFZ)
Aliyun AHAS | 阿里云应用高可用服务 | [官网](https://help.aliyun.com/product/87450.html?spm=a2c4g.11186623.6.540.11af3e04huYJhf)

## 后端技术

技术 | 说明 | 地址
----|----|----
Spring Boot | 新一代 JavaEE 开发标准 | [GitHub](https://github.com/spring-projects/spring-boot)
Spring Cloud Alibaba | 阿里巴巴基于 Spring Cloud 编程模型的微服务生态 | [GitHub](https://github.com/alibaba/spring-cloud-alibaba)
Spring Cloud Alibaba Dubbo | 与 Spring Cloud Alibaba 生态相结合的高性能 Java RPC 框架 | [GitHub](https://github.com/apache/dubbo)
Spring Cloud Gateway | 服务网关路由代理 | [GitHub](https://github.com/spring-cloud/spring-cloud-gateway)
SSM | 常用的web项目框架 | [官网](https://spring.io/)
TkMyBatis | 基于 MyBatis 二次开发的轻量级框架，用于简化 MyBatis 操作 | [GitHub](https://github.com/abel533/Mapper)
HikariCP | 号称史上最快的数据库连接池 | [GitHub](https://github.com/brettwooldridge/HikariCP)
MybatisCodeHelper | Intellij IDEA 插件，用于 MyBatis 相关代码生成 | [官网](https://plugins.jetbrains.com/plugin/9837-mybatiscodehelperpro)
PageHelper | MyBatis 分页插件 | [GitHub](https://github.com/pagehelper/Mybatis-PageHelper)
MySQL | 关系型数据库 | [官网](https://www.mysql.com/)
Redis | 非关系型数据库 | [官网](https://redis.io/)
ShowDoc | API 在线文档分享工具 | [GitHub](https://github.com/star7th/showdoc)


## 前端技术

技术 | 说明 | 地址
----|----|----
微信小程序 | 微信小程序 | [官网](https://mp.weixin.qq.com/)

## 其他技术
技术 | 说明 | 地址
----|----|----
Python | 爬虫 | 

## 备注
- 因为服务器内存原因，后端只拆分为3个大的服务（服务提供者、服务消费者、服务网关）。
- 商品秒杀测试解释：在阿里云AHAS中设置为1QPS，即1秒内只能1人访问资源，所以在测试时需要通过PostMan访问API（http://xsfno1.top/api/wechat/seckill/limit ）。
- showDoc 在线API文档地址：https://www.showdoc.cc/jieyouwechat


## 项目体验申请（可能会随时关闭）
![image](https://images.gitee.com/uploads/images/2020/0102/121549_f6fcbcec_5193480.png)