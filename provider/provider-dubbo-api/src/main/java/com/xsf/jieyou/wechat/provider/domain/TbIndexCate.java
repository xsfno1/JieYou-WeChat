package com.xsf.jieyou.wechat.provider.domain;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "tb_index_cate")
public class TbIndexCate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 标题名称
     */
    @Column(name = "`name`")
    private String name;

    /**
     * 图片路径
     */
    @Column(name = "image_src")
    private String imageSrc;

    @Column(name = "open_type")
    private String openType;

    @Column(name = "navigator_url")
    private String navigatorUrl;

    private static final long serialVersionUID = 1L;
}