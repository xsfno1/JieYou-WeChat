package com.xsf.jieyou.wechat.provider.domain;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "tb_index_swiper")
public class TbIndexSwiper implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 图片路径
     */
    @Column(name = "image_src")
    private String imageSrc;

    /**
     * 打开方式
     */
    @Column(name = "open_type")
    private String openType;

    /**
     * 商品id
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 跳转路径
     */
    @Column(name = "navigator_url")
    private String navigatorUrl;

    private static final long serialVersionUID = 1L;
}