package com.xsf.jieyou.wechat.provider.api;

import com.xsf.jieyou.wechat.provider.domain.TbIndexCate;

import java.util.List;

/**
 * 主页：导航菜单  Dubbo API
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/30 0030 1:50
 * @see com.xsf.jieyou.wechat.provider.api
 **/
public interface ProviderTbIndexCateService {
    /**
     * 查询导航菜单
     * @return {@link List<TbIndexCate>}
     */
    List<TbIndexCate> selectCateItems();
}
