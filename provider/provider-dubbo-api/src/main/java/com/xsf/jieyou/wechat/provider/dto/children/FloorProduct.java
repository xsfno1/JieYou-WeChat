package com.xsf.jieyou.wechat.provider.dto.children;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 首页楼层商品数据DTO   商品列表
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/1 0001 21:53
 * @see com.xsf.jieyou.wechat.provider.dto.children
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FloorProduct implements Serializable {

    private static final long serialVersionUID = 2065868567837503409L;

    /**
     * 名称
     */
    private String name;
    /**
     * 图片地址
     */
    private String imageSrc;
    /**
     * 打开方式
     */
    private String openType;
    /**
     * 跳转路径
     */
    private String navigatorUrl;
}
