package com.xsf.jieyou.wechat.provider.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_goods_detail")
public class TbGoodsDetail implements Serializable {
    /**
     * 商品ID
     */
    @Id
    @Column(name = "goods_id")
    @GeneratedValue(generator = "JDBC")
    private Integer goodsId;

    /**
     * 分类ID
     */
    @Column(name = "cat_id")
    private Integer catId;

    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 商品价格
     */
    @Column(name = "goods_price")
    private Integer goodsPrice;

    /**
     * 商品数量
     */
    @Column(name = "goods_number")
    private Integer goodsNumber;

    /**
     * 商品重量
     */
    @Column(name = "goods_weight")
    private Integer goodsWeight;

    /**
     * 商品大图标
     */
    @Column(name = "goods_big_logo")
    private String goodsBigLogo;

    /**
     * 商品小图标
     */
    @Column(name = "goods_small_logo")
    private String goodsSmallLogo;

    /**
     * 热门商品数
     */
    @Column(name = "hot_mumber")
    private Integer hotMumber;

    /**
     * 所属一级分类
     */
    @Column(name = "cat_one_id")
    private Integer catOneId;

    /**
     * 所属二级分类
     */
    @Column(name = "cat_two_id")
    private Integer catTwoId;

    /**
     * 所属三级分类
     */
    @Column(name = "cat_three_id")
    private Integer catThreeId;

    /**
     * 商品介绍，富文本
     */
    @Column(name = "goods_introduce")
    private String goodsIntroduce;

    private static final long serialVersionUID = 1L;
}