package com.xsf.jieyou.wechat.provider.dto;

import com.xsf.jieyou.wechat.provider.domain.TbGoodsPic;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 商品详情DTO
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 21:12
 * @see com.xsf.jieyou.wechat.provider.dto
 **/
@Data
public class GoodsDetailDTO implements Serializable {

    private static final long serialVersionUID = -430508392940499196L;

    /**
     * 商品主键
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 商品数量
     */
    private Integer goodsNumber;

    /**
     * 商品大图标
     */
    private String goodsBigLogo;

    /**
     * 商品小图标
     */
    private String goodsSmallLogo;

    /**
     * 商品介绍，富文本
     */
    private String goodsIntroduce;

    /**
     * 图片集合
     */
    private List<TbGoodsPic> pics;
}
