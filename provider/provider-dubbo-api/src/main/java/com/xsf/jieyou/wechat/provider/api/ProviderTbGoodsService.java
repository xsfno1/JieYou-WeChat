package com.xsf.jieyou.wechat.provider.api;

import com.xsf.jieyou.wechat.commons.entity.PageResult;
import com.xsf.jieyou.wechat.provider.domain.TbGoods;

/**
 * 商品，Dubbo API。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 22:14
 * @see com.xsf.jieyou.wechat.provider.api
 **/
public interface ProviderTbGoodsService {
    /**
     * 商品列表搜索，分页
     *
     * @param query           关键字
     * @param cid             分类id
     * @param pagenum         页码
     * @param pagesize        页容量
     * @param orderByProperty 排序字段
     * @return {@link PageResult<TbGoods>}
     */
    PageResult<TbGoods> searchGoods(String query, Integer cid, Integer pagenum, Integer pagesize,
                                    String orderByProperty);
}
