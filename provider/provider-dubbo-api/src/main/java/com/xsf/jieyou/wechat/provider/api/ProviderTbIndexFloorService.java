package com.xsf.jieyou.wechat.provider.api;

import com.xsf.jieyou.wechat.provider.dto.FloorDataDto;

import java.util.List;

/**
 * 主页：楼层商品数据  Dubbo API
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/1 0001 21:48
 * @see com.xsf.jieyou.wechat.provider.api
 **/
public interface ProviderTbIndexFloorService {

    /**
     * 查询首页楼层商品数据
     * @return {@link List<FloorDataDto>}
     */
    List<FloorDataDto> selectHomeFloorData();
}
