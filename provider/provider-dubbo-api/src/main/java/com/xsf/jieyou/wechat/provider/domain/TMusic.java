package com.xsf.jieyou.wechat.provider.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 老黄接口。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010
 * @see com.xsf.jieyou.wechat.provider.domain
 **/
@Data
@Table(name = "t_music")
public class TMusic implements Serializable {
    @Id
    @Column(name = "mid")
    @GeneratedValue(generator = "JDBC")
    private Integer mid;

    @Column(name = "mname")
    private String mname;

    @Column(name = "msrc")
    private String msrc;

    @Column(name = "mauto")
    private String mauto;

    @Column(name = "autosrc")
    private String autosrc;

    private static final long serialVersionUID = 1L;
}