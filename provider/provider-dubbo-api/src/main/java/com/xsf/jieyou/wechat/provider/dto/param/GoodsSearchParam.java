package com.xsf.jieyou.wechat.provider.dto.param;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 22:25
 * @see com.xsf.jieyou.wechat.provider.dto.param
 **/
@Data
public class GoodsSearchParam implements Serializable {

    private static final long serialVersionUID = -5922799178684633575L;

    /**
     * 关键字
     */
    private String query;
    /**
     * 分类id
     */
    private Integer cid;
    /**
     * 页码
     */
    private Integer pagenum;
    /**
     * 页容量
     */
    private Integer pagesize;
}
