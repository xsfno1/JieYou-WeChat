package com.xsf.jieyou.wechat.provider.domain;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "tb_index_floor_list")
public class TbIndexFloorList implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 父ID
     */
    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "`name`")
    private String name;

    /**
     * 图片路径
     */
    @Column(name = "image_src")
    private String imageSrc;

    /**
     * 打开方式
     */
    @Column(name = "open_type")
    private String openType;

    /**
     * 跳转连接
     */
    @Column(name = "navigator_url")
    private String navigatorUrl;

    private static final long serialVersionUID = 1L;
}