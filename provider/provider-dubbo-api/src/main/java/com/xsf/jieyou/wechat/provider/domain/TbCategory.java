package com.xsf.jieyou.wechat.provider.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_category")
public class TbCategory implements Serializable {
    private static final long serialVersionUID = -5773833773673601166L;
    /**
     * 主键
     */
    @Id
    @Column(name = "cat_id")
    @GeneratedValue(generator = "JDBC")
    private Integer catId;

    /**
     * 分类名称
     */
    @Column(name = "cat_name")
    private String catName;

    /**
     * 是否为父节点   1是  0否
     */
    @Column(name = "is_parent")
    private Boolean isParent;

    /**
     * 父节点ID
     */
    @Column(name = "cat_pid")
    private Integer catPid;

    /**
     * 排序等级
     */
    @Column(name = "cat_level")
    private Integer catLevel;

    /**
     * 是否已删除    0未删除  1已删除
     */
    @Column(name = "cat_deleted")
    private Boolean catDeleted;

    /**
     * 分类图标
     */
    @Column(name = "cat_icon")
    private String catIcon;

}