package com.xsf.jieyou.wechat.provider.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_seckill")
public class TbSeckill implements Serializable {

    private static final long serialVersionUID = -4406788527554679840L;

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 商品ID
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 商品图片
     */
    @Column(name = "goods_small_logo")
    private String goodsSmallLogo;

    /**
     * 秒杀价格
     */
    @Column(name = "new_price")
    private Integer newPrice;

    @Column(name = "old_price")
    private Integer oldPrice;

    /**
     * 秒杀商品容量
     */
    @Column(name = "seckill_reserve")
    private Integer seckillReserve;

    /**
     * 已买数量
     */
    @Column(name = "bought_count")
    private Integer boughtCount;

    /**
     * 场次
     */
    @Column(name = "seckill_date")
    private Integer seckillDate;

}