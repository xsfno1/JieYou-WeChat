package com.xsf.jieyou.wechat.provider.domain;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "tb_index_floor")
public class TbIndexFloor implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    @Column(name = "`name`")
    private String name;

    @Column(name = "image_src")
    private String imageSrc;

    private static final long serialVersionUID = 1L;
}