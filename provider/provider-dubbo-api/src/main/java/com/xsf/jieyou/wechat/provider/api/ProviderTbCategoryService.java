package com.xsf.jieyou.wechat.provider.api;

import com.xsf.jieyou.wechat.provider.domain.TbCategory;

import java.util.List;

/**
 * 分类。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 15:06
 * @see com.xsf.jieyou.wechat.provider.api
 **/
public interface ProviderTbCategoryService {
    /**
     * 根据父节点 ID 查询子节点
     *
     * @param pid 父节点ID
     * @return {@link List<TbCategory>}
     */
    List<TbCategory> selectCategoryByParentId(Integer pid);
}
