package com.xsf.jieyou.wechat.provider.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分类节点DTO
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 16:04
 * @see com.xsf.jieyou.wechat.provider.dto
 **/
@Data
public class CateNodeDTO implements Serializable {

    private static final long serialVersionUID = -4877103212310588663L;

    /**
     * 主键
     */
    private Integer catId;

    /**
     * 分类名称
     */
    private String catName;

    /**
     * 分类图标
     */
    private String catIcon;

    /**
     * 子节点
     */
    private List<CateNodeDTO> children;
}
