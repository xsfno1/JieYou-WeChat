package com.xsf.jieyou.wechat.provider.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 老黄接口。
 *
 * @author xsf
 * @date 2019/12/10 0010
 * @see com.xsf.jieyou.wechat.provider.domain
 * @version v1.0.0
 **/
@Data
@Table(name = "t_indeximg")
public class TIndeximg implements Serializable {
    @Id
    @Column(name = "index_id")
    @GeneratedValue(generator = "JDBC")
    private Integer indexId;

    @Column(name = "index_imgname")
    private String indexImgname;

    @Column(name = "index_imgsrc")
    private String indexImgsrc;

    private static final long serialVersionUID = 1L;
}