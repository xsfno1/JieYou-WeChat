package com.xsf.jieyou.wechat.provider.huang.api;

import com.xsf.jieyou.wechat.provider.domain.TIndeximg;

import java.util.List;

/**
 * 老黄接口。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 7:36
 * @see com.xsf.jieyou.wechat.provider.huang.api
 **/
public interface TIndeximgService {
    /**
     * 获取所有
     *
     * @return {@link List<TIndeximg>}
     */
    List<TIndeximg> selectAll();
}
