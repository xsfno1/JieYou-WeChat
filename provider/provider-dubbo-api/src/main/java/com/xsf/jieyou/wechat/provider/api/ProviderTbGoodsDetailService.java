package com.xsf.jieyou.wechat.provider.api;

import com.xsf.jieyou.wechat.provider.dto.GoodsDetailDTO;

/**
 * 商品详情
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 21:10
 * @see com.xsf.jieyou.wechat.provider.api
 **/
public interface ProviderTbGoodsDetailService {
    /**
     * 查询商品详情，By商品主键
     * @param goodsId 商品ID
     * @return {@link GoodsDetailDTO}
     */
    GoodsDetailDTO selectGoodsDetailByGid(Integer goodsId);
}
