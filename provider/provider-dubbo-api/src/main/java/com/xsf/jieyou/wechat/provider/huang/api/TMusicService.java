package com.xsf.jieyou.wechat.provider.huang.api;

import com.xsf.jieyou.wechat.provider.domain.TMusic;

import java.util.List;

/**
 * 老黄接口。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 7:38
 * @see com.xsf.jieyou.wechat.provider.huang.api
 **/
public interface TMusicService {
    /**
     * 获取所有
     *
     * @return {@link List <TMusic>}
     */
    List<TMusic> selectAll();

    /**
     * 获取单个
     *
     * @param mid {@link Integer}
     * @return {@link TMusic}
     */
    TMusic selmusBymid(Integer mid);
}
