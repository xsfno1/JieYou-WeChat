package com.xsf.jieyou.wechat.provider.dto;

import com.xsf.jieyou.wechat.provider.dto.children.FloorTitle;
import com.xsf.jieyou.wechat.provider.dto.children.FloorProduct;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 首页楼层商品数据DTO
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/1 0001 21:49
 * @see com.xsf.jieyou.wechat.provider.dto
 **/
@Data
public class FloorDataDto implements Serializable {

    private static final long serialVersionUID = -3322188840292131228L;
    /**
     * 标题
     */
    private FloorTitle floorTitle;
    /**
     * 商品列表
     */
    private List<FloorProduct> productLists;
}
