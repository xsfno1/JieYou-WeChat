package com.xsf.jieyou.wechat.provider.api;

import com.xsf.jieyou.wechat.provider.domain.TbIndexSwiper;

import java.util.List;

/**
 * 首页轮播图 Dubbo API
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/29 0029 21:31
 * @see com.xsf.jieyou.wechat.provider.api
 **/
public interface ProviderTbIndexSwiperService {
    /**
     * 查询所有
     *
     * @return {@link List}
     */
    List<TbIndexSwiper> selectAll();
}
