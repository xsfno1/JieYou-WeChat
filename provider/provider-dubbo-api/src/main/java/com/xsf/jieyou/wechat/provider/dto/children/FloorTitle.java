package com.xsf.jieyou.wechat.provider.dto.children;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 首页楼层商品数据DTO  标题
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/1 0001 21:51
 * @see com.xsf.jieyou.wechat.provider.dto.children
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FloorTitle implements Serializable {


    private static final long serialVersionUID = -2884503121688370621L;
    /**
     * 名称
     */
    private String name;
    /**
     * 图片地址
     */
    private String imageSrc;


}
