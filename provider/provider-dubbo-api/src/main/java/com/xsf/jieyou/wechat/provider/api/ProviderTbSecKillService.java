package com.xsf.jieyou.wechat.provider.api;

import com.xsf.jieyou.wechat.provider.domain.TbSeckill;

import java.util.List;

/**
 * 商品秒杀
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 1:43
 * @see com.xsf.jieyou.wechat.provider.api
 **/
public interface ProviderTbSecKillService {
    /**
     * 根据场次获取秒杀列表
     *
     * @param secKillDate 场次
     * @return {@link  List<TbSeckill>}
     */
    List<TbSeckill> selectSecKillList(Integer secKillDate);

    /**
     * 测试限流功能
     *
     * @return {@link  TbSeckill}
     */
    TbSeckill currentLimiting();
}
