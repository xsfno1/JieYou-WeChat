package com.xsf.jieyou.wechat.provider.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_goods_pic")
public class TbGoodsPic implements Serializable {
    @Id
    @Column(name = "pics_id")
    @GeneratedValue(generator = "JDBC")
    private Integer picsId;

    /**
     * 商品主键
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    @Column(name = "pics_big")
    private String picsBig;

    @Column(name = "pics_mid")
    private String picsMid;

    @Column(name = "pics_sma")
    private String picsSma;

    @Column(name = "pics_big_url")
    private String picsBigUrl;

    @Column(name = "pics_mid_url")
    private String picsMidUrl;

    @Column(name = "pics_sma_url")
    private String picsSmaUrl;

    private static final long serialVersionUID = 1L;
}