package com.xsf.jieyou.wechat.provider.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_goods")
public class TbGoods implements Serializable {
    /**
     * 商品ID
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 分类ID
     */
    @Column(name = "cat_id")
    private Integer catId;

    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 商品价格
     */
    @Column(name = "goods_price")
    private Integer goodsPrice;

    /**
     * 商品数量
     */
    @Column(name = "goods_number")
    private Integer goodsNumber;

    /**
     * 商品大图标
     */
    @Column(name = "goods_big_logo")
    private String goodsBigLogo;

    /**
     * 商品小图标
     */
    @Column(name = "goods_small_logo")
    private String goodsSmallLogo;

    private static final long serialVersionUID = 1L;
}