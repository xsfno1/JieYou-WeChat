package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbSecKillService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 18:58
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProviderTbSecKillTests {
    @Resource
    private ProviderTbSecKillService providerTbSecKillService;

    /**
     * 测试查询所有
     */
    @Test
    public void selectAll(){
        System.out.println(this.providerTbSecKillService.selectSecKillList(8));
    }
}
