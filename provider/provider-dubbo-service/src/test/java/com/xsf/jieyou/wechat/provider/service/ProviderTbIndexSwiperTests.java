package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbIndexSwiperService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/29 0029 21:44
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProviderTbIndexSwiperTests {
    @Resource
    private ProviderTbIndexSwiperService providerTbIndexSwiperService;

    /**
     * 测试查询所有
     */
    @Test
    public void testSelectAll(){
        System.out.println(this.providerTbIndexSwiperService.selectAll());
    }
}
