package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbCategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 15:17
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProviderTbCategoryTests {
    @Resource
    private ProviderTbCategoryService providerTbCategoryService;

    @Test
    public void select(){
        System.out.println(this.providerTbCategoryService.selectCategoryByParentId(0));
    }
}
