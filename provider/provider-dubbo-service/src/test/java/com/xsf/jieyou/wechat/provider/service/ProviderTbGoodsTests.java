package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbGoodsService;
import com.xsf.jieyou.wechat.provider.dto.param.GoodsSearchParam;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 18:58
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProviderTbGoodsTests {
    @Resource
    private ProviderTbGoodsService providerTbGoodsService;

    /**
     * 测试查询所有
     */
    @Test
    public void selectAll(){
        GoodsSearchParam param = new GoodsSearchParam();
        param.setPagenum(2);
        param.setPagesize(3);

      //  System.out.println(this.providerTbGoodsService.searchGoods(param));
    }
}
