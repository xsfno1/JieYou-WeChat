package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbIndexFloor;
import tk.mybatis.mapper.common.Mapper;

public interface TbIndexFloorMapper extends Mapper<TbIndexFloor> {
}