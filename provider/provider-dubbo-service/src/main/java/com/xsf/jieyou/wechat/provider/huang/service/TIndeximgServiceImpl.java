package com.xsf.jieyou.wechat.provider.huang.service;

import com.xsf.jieyou.wechat.provider.domain.TIndeximg;
import com.xsf.jieyou.wechat.provider.huang.api.TIndeximgService;
import com.xsf.jieyou.wechat.provider.mapper.TIndeximgMapper;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 老黄接口。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 7:37
 * @see com.xsf.jieyou.wechat.provider.huang.service
 **/
@Service(version = "1.0.0")
public class TIndeximgServiceImpl implements TIndeximgService {
    @Resource
    private TIndeximgMapper tIndeximgMapper;

    /**
     * 获取所有
     *
     * @return {@link List<TIndeximg>}
     */
    @Override
    public List<TIndeximg> selectAll() {
        return this.tIndeximgMapper.selectAll();
    }
}
