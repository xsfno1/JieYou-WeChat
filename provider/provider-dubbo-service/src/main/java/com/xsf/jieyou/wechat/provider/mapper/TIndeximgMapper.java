package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TIndeximg;
import tk.mybatis.mapper.common.Mapper;

public interface TIndeximgMapper extends Mapper<TIndeximg> {
}