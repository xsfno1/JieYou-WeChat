package com.xsf.jieyou.wechat.provider.fallback;

import com.xsf.jieyou.wechat.provider.domain.TbSeckill;
import lombok.extern.slf4j.Slf4j;

/**
 * 熔断器 Dubbo Sentinel。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/5 0005 22:36
 * @see com.xsf.jieyou.wechat.provider.fallback
 **/
@Slf4j
public class SeckillFallback {
    /**
     * 熔断方法，注意：必须为静态方法 static。
     *
     * @param ex {@code Throwable} 异常信息
     * @return {@link TbSeckill} 熔断后的固定结果
     */
    public static TbSeckill currentLimiting(Throwable ex) {
        log.warn("Invoke currentLimiting: " + ex.getClass().getTypeName());
        ex.printStackTrace();
        return null;
    }
}
