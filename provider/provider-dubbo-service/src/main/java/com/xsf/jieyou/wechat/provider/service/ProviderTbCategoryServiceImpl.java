package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbCategoryService;
import com.xsf.jieyou.wechat.provider.domain.TbCategory;
import com.xsf.jieyou.wechat.provider.mapper.TbCategoryMapper;
import org.apache.dubbo.config.annotation.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * 分类。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 15:06
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@Service(version = "1.0.0")
public class ProviderTbCategoryServiceImpl implements ProviderTbCategoryService {
    @Resource
    private TbCategoryMapper tbCategoryMapper;

    /**
     * 根据父节点 ID 查询子节点
     *
     * @param pid 父节点ID
     * @return {@link List<TbCategory>}
     */
    @Override
    public List<TbCategory> selectCategoryByParentId(Integer pid) {
        Example example = new Example(TbCategory.class);
        // catDeleted ：是否已删除    0未删除  1已删除
        // catPid：父节点ID
        example.createCriteria().andEqualTo("catPid", pid).andEqualTo("catDeleted", false);
        // 排序
        example.orderBy("catLevel").asc();
        return this.tbCategoryMapper.selectByExample(example);
    }
}
