package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbGoodsDetailService;
import com.xsf.jieyou.wechat.provider.domain.TbGoodsDetail;
import com.xsf.jieyou.wechat.provider.domain.TbGoodsPic;
import com.xsf.jieyou.wechat.provider.dto.GoodsDetailDTO;
import com.xsf.jieyou.wechat.provider.mapper.TbGoodsDetailMapper;
import com.xsf.jieyou.wechat.provider.mapper.TbGoodsPicMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品详情
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/3 0003 21:17
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@Service(version = "1.0.0")
public class ProviderTbGoodsDetailServiceImpl implements ProviderTbGoodsDetailService {
    @Resource
    private TbGoodsDetailMapper tbGoodsDetailMapper;
    @Resource
    private TbGoodsPicMapper tbGoodsPicMapper;

    /**
     * 查询商品详情，By商品主键
     *
     * @param goodsId 商品ID
     * @return {@link GoodsDetailDTO}
     */
    @Override
    public GoodsDetailDTO selectGoodsDetailByGid(Integer goodsId) {
        // 1 查询 TbGoodsDetail
        TbGoodsDetail tbGoodsDetail = this.tbGoodsDetailMapper.selectByPrimaryKey(goodsId);
        // 2 查询 TbGoodsPic
        Example example = new Example(TbGoodsPic.class);
        example.createCriteria().andEqualTo("goodsId", goodsId);
        List<TbGoodsPic> tbGoodsPics = this.tbGoodsPicMapper.selectByExample(example);
        // 3 数据加工封装
        GoodsDetailDTO goodsDetailDTO = new GoodsDetailDTO();
        BeanUtils.copyProperties(tbGoodsDetail,goodsDetailDTO);
        goodsDetailDTO.setPics(tbGoodsPics);

        return goodsDetailDTO;
    }
}
