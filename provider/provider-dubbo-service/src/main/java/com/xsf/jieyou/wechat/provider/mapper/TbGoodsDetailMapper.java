package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbGoodsDetail;
import tk.mybatis.mapper.common.Mapper;

public interface TbGoodsDetailMapper extends Mapper<TbGoodsDetail> {
}