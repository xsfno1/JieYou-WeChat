package com.xsf.jieyou.wechat.provider.huang.service;

import com.xsf.jieyou.wechat.provider.domain.TMusic;
import com.xsf.jieyou.wechat.provider.huang.api.TMusicService;
import com.xsf.jieyou.wechat.provider.mapper.TMusicMapper;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 老黄接口。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/10 0010 7:38
 * @see com.xsf.jieyou.wechat.provider.huang.service
 **/
@Service(version = "1.0.0")
public class TMusicServiceImpl implements TMusicService {
    @Resource
    private TMusicMapper tMusicMapper;

    /**
     * 获取所有
     *
     * @return {@link List <TMusic>}
     */
    @Override
    public List<TMusic> selectAll() {
        return this.tMusicMapper.selectAll();
    }

    /**
     * 获取单个
     *
     * @param mid {@link Integer}
     * @return {@link TMusic}
     */
    @Override
    public TMusic selmusBymid(Integer mid) {
        return this.tMusicMapper.selectByPrimaryKey(mid);
    }
}
