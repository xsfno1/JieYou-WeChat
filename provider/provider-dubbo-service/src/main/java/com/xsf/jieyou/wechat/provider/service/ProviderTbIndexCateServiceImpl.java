package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbIndexCateService;
import com.xsf.jieyou.wechat.provider.domain.TbIndexCate;
import com.xsf.jieyou.wechat.provider.mapper.TbIndexCateMapper;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 主页：导航菜单  Dubbo 实现类
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/30 0030 1:50
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@Service(version = "1.0.0")
public class ProviderTbIndexCateServiceImpl implements ProviderTbIndexCateService {
    @Resource
    private TbIndexCateMapper tbIndexCateMapper;

    /**
     * 查询导航菜单
     *
     * @return {@link List<TbIndexCate>}
     */
    @Override
    public List<TbIndexCate> selectCateItems() {
        return this.tbIndexCateMapper.selectAll();
    }
}
