package com.xsf.jieyou.wechat.provider.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.xsf.jieyou.wechat.provider.api.ProviderTbSecKillService;
import com.xsf.jieyou.wechat.provider.domain.TbSeckill;
import com.xsf.jieyou.wechat.provider.fallback.SeckillFallback;
import com.xsf.jieyou.wechat.provider.mapper.TbSeckillMapper;
import org.apache.dubbo.config.annotation.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品秒杀
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 18:55
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@Service(version = "1.0.0")
public class ProviderTbSecKillServiceImpl implements ProviderTbSecKillService {
    @Resource
    private TbSeckillMapper tbSeckillMapper;

    /**
     * 根据场次获取秒杀列表
     *
     * @param secKillDate 场次
     * @return {@link  List<TbSeckill>}
     */
    @Override
    public List<TbSeckill> selectSecKillList(Integer secKillDate) {
        Example example = new Example(TbSeckill.class);
        example.createCriteria().andEqualTo("seckillDate", secKillDate);
        return this.tbSeckillMapper.selectByExample(example);
    }

    /**
     * 测试限流功能
     *
     * @return {@link  TbSeckill}
     */
    @Override
    @SentinelResource(value = "seckill", fallbackClass = SeckillFallback.class, fallback =
            "currentLimiting")
    public TbSeckill currentLimiting() {
        TbSeckill tbSeckill = new TbSeckill();
        tbSeckill.setId(8848);
        tbSeckill.setGoodsId(8888);
        tbSeckill.setGoodsName("邓紫棋");
        tbSeckill.setNewPrice(8848);
        tbSeckill.setOldPrice(8848);
        return tbSeckill;
    }
}
