package com.xsf.jieyou.wechat.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * Dubbo 服务提供者，启动类。
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/15 0015 2:54
 * @see com.xsf.jieyou.wechat.provider
 **/
@SpringBootApplication
@MapperScan(basePackages = "com.xsf.jieyou.wechat.provider.mapper")
public class ProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }
}
