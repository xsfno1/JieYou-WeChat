package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbGoods;
import tk.mybatis.mapper.common.Mapper;

public interface TbGoodsMapper extends Mapper<TbGoods> {
}