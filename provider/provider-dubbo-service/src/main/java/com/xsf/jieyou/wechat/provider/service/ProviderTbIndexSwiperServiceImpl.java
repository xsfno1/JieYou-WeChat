package com.xsf.jieyou.wechat.provider.service;

import com.xsf.jieyou.wechat.provider.api.ProviderTbIndexSwiperService;
import com.xsf.jieyou.wechat.provider.domain.TbIndexSwiper;
import com.xsf.jieyou.wechat.provider.mapper.TbIndexSwiperMapper;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 首页轮播图 Dubbo 实现类
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/11/29 0029 21:31
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@Service(version = "1.0.0")
public class ProviderTbIndexSwiperServiceImpl implements ProviderTbIndexSwiperService {
    @Resource
    private TbIndexSwiperMapper tbIndexSwiperMapper;

    /**
     * 查询所有
     *
     * @return {@link List}
     */
    @Override
    public List<TbIndexSwiper> selectAll() {
        return this.tbIndexSwiperMapper.selectAll();
    }
}
