package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TMusic;
import tk.mybatis.mapper.common.Mapper;

public interface TMusicMapper extends Mapper<TMusic> {
}