package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbGoodsPic;
import tk.mybatis.mapper.common.Mapper;

public interface TbGoodsPicMapper extends Mapper<TbGoodsPic> {
}