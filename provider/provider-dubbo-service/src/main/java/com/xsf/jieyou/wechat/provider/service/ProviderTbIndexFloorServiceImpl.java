package com.xsf.jieyou.wechat.provider.service;

import com.google.common.collect.Lists;
import com.xsf.jieyou.wechat.provider.api.ProviderTbIndexFloorService;
import com.xsf.jieyou.wechat.provider.domain.TbIndexFloor;
import com.xsf.jieyou.wechat.provider.domain.TbIndexFloorList;
import com.xsf.jieyou.wechat.provider.dto.FloorDataDto;
import com.xsf.jieyou.wechat.provider.dto.children.FloorProduct;
import com.xsf.jieyou.wechat.provider.dto.children.FloorTitle;
import com.xsf.jieyou.wechat.provider.mapper.TbIndexFloorListMapper;
import com.xsf.jieyou.wechat.provider.mapper.TbIndexFloorMapper;
import org.apache.dubbo.config.annotation.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * Description:TODO
 * </p>
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/1 0001 21:57
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@Service(version = "1.0.0")
public class ProviderTbIndexFloorServiceImpl implements ProviderTbIndexFloorService {
    @Resource
    private TbIndexFloorMapper tbIndexFloorMapper;
    @Resource
    private TbIndexFloorListMapper tbIndexFloorListMapper;

    /**
     * 查询首页楼层商品数据
     *
     * @return {@link List<FloorDataDto>}
     */
    @Override
    public List<FloorDataDto> selectHomeFloorData() {
        List<FloorDataDto> list = Lists.newArrayList();

        // 获取所有楼层标题
        List<TbIndexFloor> tbIndexFloors = this.tbIndexFloorMapper.selectAll();
        // 获取所有楼层标题的商品列表
        tbIndexFloors.forEach(tbIndexFloor -> {
            FloorDataDto floorDataDto = new FloorDataDto();

            Example example = new Example(TbIndexFloorList.class);
            example.createCriteria().andEqualTo("parentId", tbIndexFloor.getId());
            List<TbIndexFloorList> tbIndexFloorLists = this.tbIndexFloorListMapper.selectByExample(example);

            // 数据封装
            floorDataDto.setFloorTitle(new FloorTitle(tbIndexFloor.getName(), tbIndexFloor.getImageSrc()));
            List<FloorProduct> floorProducts = Lists.newArrayList();
            tbIndexFloorLists.forEach(tbIndexFloorList -> {
                floorProducts.add(new FloorProduct(tbIndexFloorList.getName(), tbIndexFloorList.getImageSrc(),
                        tbIndexFloorList.getOpenType(), tbIndexFloorList.getNavigatorUrl()));
            });
            floorDataDto.setProductLists(floorProducts);

            list.add(floorDataDto);
        });

        return list;
    }
}
