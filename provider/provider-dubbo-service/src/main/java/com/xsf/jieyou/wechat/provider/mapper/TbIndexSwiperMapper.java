package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbIndexSwiper;
import tk.mybatis.mapper.common.Mapper;

public interface TbIndexSwiperMapper extends Mapper<TbIndexSwiper> {
}