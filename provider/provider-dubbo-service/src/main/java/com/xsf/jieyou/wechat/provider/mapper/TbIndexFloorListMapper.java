package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbIndexFloorList;
import tk.mybatis.mapper.common.Mapper;

public interface TbIndexFloorListMapper extends Mapper<TbIndexFloorList> {
}