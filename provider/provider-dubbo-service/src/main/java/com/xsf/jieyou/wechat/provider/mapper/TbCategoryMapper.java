package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbCategory;
import tk.mybatis.mapper.common.Mapper;

public interface TbCategoryMapper extends Mapper<TbCategory> {
}