package com.xsf.jieyou.wechat.provider.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.xsf.jieyou.wechat.commons.entity.PageResult;
import com.xsf.jieyou.wechat.provider.api.ProviderTbGoodsService;
import com.xsf.jieyou.wechat.provider.domain.TbGoods;
import com.xsf.jieyou.wechat.provider.mapper.TbGoodsMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/2 0002 22:30
 * @see com.xsf.jieyou.wechat.provider.service
 **/
@Service(version = "1.0.0")
public class ProviderTbGoodsServiceImpl implements ProviderTbGoodsService {
    @Resource
    private TbGoodsMapper tbGoodsMapper;

    /**
     * 商品列表搜索，分页
     *
     * @param query           关键字
     * @param cid             分类id
     * @param pagenum         页码
     * @param pagesize        页容量
     * @param orderByProperty 排序字段
     * @return {@link PageResult<TbGoods>}
     */
    @Override
    public PageResult<TbGoods> searchGoods(String query, Integer cid, Integer pagenum, Integer pagesize,
                                           String orderByProperty) {
        PageHelper.startPage(pagenum, pagesize);

        Example example = new Example(TbGoods.class);
        example.createCriteria()
                .andLike("goodsName", "%" + query + "%")
                .andEqualTo("catId", cid);
        if (!StringUtils.isEmpty(orderByProperty)) {
            example.orderBy(orderByProperty).asc();
        }
        List<TbGoods> list = this.tbGoodsMapper.selectByExample(example);

        PageInfo<TbGoods> pageInfo = new PageInfo<>(list);

        // ###########################   未知BUG(PageHelp)修复方案 Start  ############################################
        // BUG 说明：如果直接传输 list 会出现分页器 Page 类未找到错误
        //       Caused by: java.lang.ClassNotFoundException: com.github.pagehelper.Page
        //       Caused by: com.esotericsoftware.kryo.KryoException: Unable to find class: com.github.pagehelper.Page
        // 方案说明：而将原来的 list 复制到新建的 newList 上进行传输则不会出现以上错误
        List<TbGoods> newList = Lists.newArrayList();
        list.forEach(tbGoods -> {
            TbGoods tempGoods = new TbGoods();
            BeanUtils.copyProperties(tbGoods, tempGoods);
            newList.add(tbGoods);
        });
        PageResult<TbGoods> pageResult = new PageResult<>();
        pageResult.setTotalPage(pageInfo.getPages());
        pageResult.setPageIndex(pageInfo.getPageNum());
        pageResult.setResult(newList);
        // ###########################   未知BUG(PageHelp)修复方案 End  ############################################

        return pageResult;
    }
}
