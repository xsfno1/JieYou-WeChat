package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbSeckill;
import tk.mybatis.mapper.common.Mapper;

public interface TbSeckillMapper extends Mapper<TbSeckill> {
}