package com.xsf.jieyou.wechat.provider.mapper;

import com.xsf.jieyou.wechat.provider.domain.TbIndexCate;
import tk.mybatis.mapper.common.Mapper;

public interface TbIndexCateMapper extends Mapper<TbIndexCate> {
}