package com.xsf.jieyou.wechat.provider.configuration;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置 SentinelResourceAspect（需要先引入 Spring AOP 并开启）
 *
 * @author xsf
 * @version v1.0.0
 * @date 2019/12/5 0005 22:10
 * @see com.xsf.jieyou.wechat.provider.configuration
 **/
@Configuration
public class SentinelAspectConfiguration {
    @Bean
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }
}
